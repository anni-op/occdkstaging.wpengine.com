<?php if(!defined('EVENTS') || (defined('EVENTS') && !EVENTS)): header("HTTP/1.0 404 Not Found"); get_template_part('404'); die(); endif; ?>
<?php get_header(); ?>
	
<section id="content" class="archive">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
		<h1><?php _e('Events', 'onlineplus-general'); ?></h1>
		<?php m('events_archive'); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer();