/*
    Name: OP-Gulp
    Platform: Wordpress
    Author: Onlineplus
    Version: 1.2.0
    Changelog: 
    1.2.0 = Added combining javascript files into one.
*/

// Require all dev dependencies.
var gulp = require('gulp');
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var sort = require('gulp-sort');
var filter = require('gulp-filter');
// CSS modules.
var cleanCSS = require('gulp-clean-css');
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
// JS modules.
var minify = require('gulp-minify');
var concat = require('gulp-concat');
//BrowserSync 
var browserSync = require('browser-sync').create();
//Php hint / format
var phpcbf = require('gulp-phpcbf');
var phpcs = require('gulp-phpcs');
//Utility 
var sourcemaps = require('gulp-sourcemaps'); // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css)
var notify = require('gulp-notify'); // Sends message notification to you

//Browsersync 
var devURL = 'https://dev.wpengine.com/'; //Your dev url localhost/customer || customer.dev2.onlineplus.dk

//Project Style files
var styleSRC = './assets/css/scss/custom.scss'; // Path to main .scss file.
var styleDestination = './assets/css/'; // Path to place the compiled CSS file.
var styleSRC1 = './assets/css/scss/admin-style.scss'; // Path to main .scss file.

//Project JS files
var jsCustomSRC = './assets/js/custom/*.js'; // Path to JS custom scripts folder.
var jsCustomDestination = './assets/js/'; // Path to place the compiled JS custom scripts file.
var jsExclude = '!assets/js/{*.min.js,**/*.min.js}';

//Project watch list 
var styleWatchFiles = './assets/css/scss/*.scss'; // Path to all *.scss files inside css folder and inside them.
var customJSWatchFiles = './assets/js/custom/*.js'; // Path to all custom JS files.
var projectPHPWatchFiles = './**/*.php'; // Path to all PHP files.


// Gulp tasks start here

//Styles tasks 
//gulp.task('build-sass', gulp.series('del', function() { 
gulp.task('build-sass', function () {
    return gulp.src(styleSRC)
        .pipe(sort())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
            autoprefixer({
                browsers: ['last 2 versions']
            })
        ]))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(styleDestination))
        .pipe(filter('**/*.css'))
        .pipe(browserSync.stream())
        .pipe(notify({
            message: 'TASK: "Styles" Completed! 💯',
            onLast: true
        }))



        // gulp.src(styleSRC1)
        //     .pipe(sort())
        //     .pipe(sourcemaps.init())
        //     .pipe(sass().on('error', sass.logError))
        //     .pipe(postcss([
        //         autoprefixer({
        //             browsers: ['> 5% in US']
        //         })
        //     ]))
        //     .pipe(cleanCSS({
        //         compatibility: 'ie8'
        //     }))
        //     .pipe(sourcemaps.write('.'))
        //     .pipe(gulp.dest(styleDestination))
        //     .pipe(filter('**/*.css'))
        //     .pipe(browserSync.stream())
        //     .pipe(notify({
        //         message: 'TASK: "Admin style" Completed! 💯',
        //         onLast: true
        //     }))

});

// Minify JS Task
//gulp.task('build-js', gulp.series('del', function() { 
gulp.task('build-js', function () {
    return gulp.src(jsCustomSRC)
        .pipe(concat('custom.js'))
        .pipe(sort())
        .pipe(minify({
            ext: {
                src: jsCustomSRC,
                min: '.min.js'
            },
            noSource: true
        }))
        .pipe(gulp.dest(jsCustomDestination))
        .pipe(notify({
            message: 'TASK: "MinifyJS" Completed! 💯',
            onLast: true
        }));
});

/**
 * Get the name of the projects base directory.
 *
 * @param  {Function} cb Callback function.
 */
//gulp.task('base-dir', gulp.series('del', function() { 
gulp.task('base-dir', function (cb) {
    git.revParse({
        args: '--show-toplevel',
        quiet: true
    }, function (err, dir) {
        if (err) {
            console.error((err.message).red);
            return cb();
        } else {
            base_dir = dir;
            return cb();
        }
    });
});

//Run PHP Code beautifier. 
gulp.task('phpcbf', gulp.series('base-dir', function() { 
//gulp.task('phpcbf', ['base-dir'], function () {
    return diff_files("ACM", "'*.php'", function (err, files) {
        if (err) return err;
        gulp.src(files)
            .pipe(phpcbf({
                bin: base_dir + '/vendor/bin/phpcbf'
            }))
            .on('error', console.error)
            .pipe(gulp.dest(function (file) {
                return file.base;
            }));
    });
}));
//Run PHP Code sniffer
gulp.task('phpcs', gulp.series('base-dir', function() { 
//gulp.task('phpcs', ['base-dir'], function () {
    diff_files("ACM", "'*.php'", function (err, files) {
        if (err) {
            throw err;
        }

        return gulp.src(files)
            // Validate files using PHP Code Sniffer
            .pipe(phpcs({
                bin: base_dir + '/vendor/bin/phpcs'
            }))
            // Log all problems that was found
            .pipe(phpcs.reporter('log'));
    });
}));

//gulp.task('composer-install', gulp.series('del', function(cb) { 
gulp.task('composer-install', function (cb) {
    //Install composer packages
    return shell_exec('composer install', cb);
});

//gulp.task('browser-sync', gulp.series('del', function() { 
// gulp.task('browser-sync', function () {
//     browserSync.init({
//         proxy: devURL,
//         open: true,
//         injectChanges: true,
//     });
// });

// gulp.task('default', gulp.series(gulp.parallel('build-sass', 'build-js', 'browser-sync'), function() { 
// //gulp.task('default', ['build-sass', 'build-js', 'browser-sync'], function () {
    
//     gulp.watch(styleWatchFiles, gulp.series('build-sass') );
//     gulp.watch(customJSWatchFiles,  gulp.series('build-js') );
//     gulp.watch(projectPHPWatchFiles, function () {
//         browserSync.reload();
//     }); 
   
// }));


gulp.task('default', gulp.series(gulp.parallel('build-sass', 'build-js'), function() { 

    browserSync.init({
        proxy: devURL,
        open: true,
        injectChanges: true,
    });

    gulp.watch(styleWatchFiles, gulp.series('build-sass')).on('change', browserSync.reload);
    gulp.watch(customJSWatchFiles, gulp.series('build-js')).on('change', browserSync.reload);
    gulp.watch(projectPHPWatchFiles).on('change', browserSync.reload);
    
    return

}));

