<?php get_header(); ?>
	
<section id="content" class="event_archive">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 event_indledning">
                <?php the_field('event_indledning', 'option'); ?>
            </div>

                <?php

                    $the_query = new WP_Query( array(  'post_type' => 'events', 'posts_per_page' => '-1'));
                    if ( $the_query->have_posts() ) :

                        echo '<div class="col-xs-12 event_sortering">';
                        
                        $terms = get_terms( 'events-kategori', array(
                            'hide_empty' => true,
                        ) );
                        $terms_list =  implode(' ', array_map(function ($entry) {
                            return '<span class="button" data-filter=".'.$entry->slug.'">'.$entry->name.'</span>';
                          }, $terms));
                      
                        echo '<span class="button" data-filter="all">'.__('Alle', 'onlineplus-general').'</span> ';
                        echo $terms_list;
                       
                        echo '</div>';

                        echo '<div class="event_mixer">';
                        while ( $the_query->have_posts() ) : $the_query->the_post();
                        
                            get_template_part('loop','event');
                        endwhile;
                        echo '</div>';
                        wp_reset_postdata();
                    else:
                        echo '<div class="col-xs-12 no-events">'.__('Ingen kommende events', 'onlineplus-general').'</div>';
                    endif;

                ?>
           
        </div>
    </div>
</section>

<?php get_footer();