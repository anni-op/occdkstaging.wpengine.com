
<?php get_header(); ?>

<section id="content" class="search">
    <div class="container">
        <div class="row">

			<?php if (have_posts()): ?>
				<div class="col-xs-12">
				<h1><?php printf(__('Results for &ldquo;%s&rdquo;', 'onlineplus-general'), '<span>' . get_search_query() . '</span>'); ?></h1>
				</div>
				<?php while (have_posts()) : the_post(); ?>

					<article class="col-xs-12  post">
						<hr>
						<h3><?php the_title(); ?></h3>
						<p class="date"><?php echo get_the_date('d-m-Y'); ?></p>
						
						
						<?php
							if('page' === get_post_type()):
							
							if( have_rows('sektioner') ):

							// loop through the rows of data
							while ( have_rows('sektioner') ) : the_row();

								if( get_row_layout() == 'text-aside' || get_row_layout() == 'text' ):
									
									echo ''.substr(strip_tags (get_sub_field('indhold')), 0, 300).'...';

									break;
								endif;

									

								endwhile;

							else :

								// no layouts found

							endif;
						else:
							echo ''.substr(strip_tags(get_the_content()), 0, 300).'...';
						endif;
						?>
						<p class="read_more"><a href="<?php the_permalink(); ?>"><?php _e('Read more', 'onlineplus-general'); ?></a></p>
					
					</article>

					

				<?php endwhile; ?>

				<div class="col-xs-12 search_pagination">
					<hr>
				<?php if ( get_previous_posts_link() ) : ?>
					<div class="nav-next pull-left"><?php previous_posts_link( '<i class="fas fa-long-arrow-left"></i> '.__( 'Forrige side', 'onlineplus-general' ) ); ?></div>
				<?php endif; ?>
				<?php if ( get_next_posts_link() ) : ?>
					<div class="nav-previous pull-right"><?php next_posts_link( __( 'Næste side', 'onlineplus-general' ).'<i class="fas fa-long-arrow-right"></i>' ); ?></div>
				<?php endif; ?>

				
				</div>
							

			<?php else: ?>
				<article class="col-xs-12 post">
					<h1><?php _e('Nothing found...', 'onlineplus-general'); ?></h1>
					<p><?php _e('Sorry, but your search yielded no results. Try other keywords...', 'onlineplus-general'); ?></p>
				</article>
			<?php endif; ?>
        </div>
    </div>
</section>

<?php
get_footer();
