<?php // Template name: Kontaktside?>
<?php get_header(); ?>

<?php //m('banner', array('banner_type' => 'full_width'));?>
<?php //m('google_map');?>

<section id="content" <?php post_class(array( 'page', 'contact')); ?>>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-3 col-md-push-9">
				<?php m('firma_information', array('overskrift' => 'Kontakt')); ?>
				<?php m('sidebar'); ?>
			</div>
			<div class="col-xs-12 col-sm-8 col-sm-pull-8 col-md-9 col-md-pull-3">
				<?php
                wp_reset_query();
                if (have_posts()) : while (have_posts()) : the_post();
                        the_content();
                    endwhile;
                endif;
                ?>

				<?php
                if (get_field('op_kontaktformular')) :
                    echo FrmFormsController::get_form_shortcode(array(
                        'id' => get_field('op_kontaktformular')
                    ));
                endif;
                ?>
			</div>
		</div>
		<?php m('gallery', array('per_row' => 6, 'new_gallery' => true)); ?>
	</div>
</section>

<?php
get_footer();
