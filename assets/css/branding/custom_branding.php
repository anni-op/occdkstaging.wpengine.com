<?php

// Admin OP tema
function add_color_scheme() {
	wp_admin_css_color(
		'onlineplus',
		__( 'OnlinePlus', 'onlineplus-general' ),
		rtrim(get_template_directory_uri(), '/') . '/core/onlineplus-admin.css',
		array( '#464646', '#595959', '#69a366', '#4db848' )
	);
}
add_action( 'admin_init', 'add_color_scheme' );

function set_default_admin_color($user_id) {
    $args = array(
        'ID' => $user_id,
        'admin_color' => 'onlineplus'
    );
    wp_update_user( $args );
}
add_action('user_register', 'set_default_admin_color');

function custom_admin_styles(){
	global $current_user;
    echo '<style type="text/css">
	#wp-admin-bar-wp-logo .ab-item .ab-icon, #wpadminbar>#wp-toolbar>#wp-admin-bar-root-default li:first-child + li .ab-icon {
	background-image: url(' . get_bloginfo('template_directory') . '/admin/images/favicon.ico) !important;
	background-position: 0px 6px !important;
	background-size: 20px 20px !important;
	background-repeat: no-repeat; }
	#wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon:before{content:\'\' !important;}
	#wpadminbar>#wp-toolbar>#wp-admin-bar-root-default .ab-icon, #wpadminbar .ab-icon{width: 20px; height: 20px;}
	@media screen and (max-width: 782px){
		#wp-admin-bar-wp-logo .ab-item .ab-icon, #wpadminbar>#wp-toolbar>#wp-admin-bar-root-default li:first-child + li .ab-icon {background-size: 26px 26px !important;background-position: 10px 10px !important;}
		#wpadminbar>#wp-toolbar>#wp-admin-bar-root-default .ab-icon, #wpadminbar .ab-icon{width: 46px; height: 46px;}
	}
	.op_msg{padding: 10px;background:#1e2a29;color: #eee !important;font-size: 18px;}
	.dashicons-admin-post:before{content: "\f327";}
	.toplevel_page_acf-options .dashicons-admin-generic:before{content: "\f319";}
	#welcome-panel + #dashboard-widgets-wrap, #welcome-panel .welcome-panel-close{display: none !important;}
	.wp-pointer-top .wp-pointer-arrow-inner, .wp-pointer-undefined .wp-pointer-arrow-inner{border-bottom-color: #595959;}
	.wp-pointer-content h3{border-color: #595959;}
	
	a, a:active, a:focus, #adminmenu li .wp-submenu ul.admin-menu-tree-page-tree li ul li li a, #adminmenu li .wp-submenu ul.admin-menu-tree-page-tree li ul li li a:active, #adminmenu li .wp-submenu ul.admin-menu-tree-page-tree li ul li li li a, #adminmenu li .wp-submenu ul.admin-menu-tree-page-tree li ul li li li a:active{color: #279727;} a:hover, #adminmenu li .wp-submenu ul.admin-menu-tree-page-tree li ul li a:hover, #adminmenu .wp-submenu a:focus, #adminmenu .wp-has-current-submenu .wp-submenu a:focus, .folded #adminmenu .wp-has-current-submenu .wp-submenu a:focus, #adminmenu a.wp-has-current-submenu:focus + .wp-submenu a:focus, #adminmenu .wp-has-current-submenu.opensub .wp-submenu a:focus, #adminmenu .wp-submenu a:hover, #adminmenu .wp-has-current-submenu .wp-submenu a:hover, .folded #adminmenu .wp-has-current-submenu .wp-submenu a:hover, #adminmenu a.wp-has-current-submenu:focus + .wp-submenu a:hover, #adminmenu .wp-has-current-submenu.opensub .wp-submenu a:hover, #adminmenu li .wp-submenu ul.admin-menu-tree-page-tree li ul li li a:hover, #adminmenu li .wp-submenu ul.admin-menu-tree-page-tree li ul li li li a:hover, #adminmenu .wp-submenu li.current a:hover, #adminmenu a.wp-has-current-submenu:focus + .wp-submenu li.current a:hover, #adminmenu .wp-has-current-submenu.opensub .wp-submenu li.current a:hover, #adminmenu .wp-submenu li.current a:focus, #adminmenu a.wp-has-current-submenu:focus + .wp-submenu li.current a:focus, #adminmenu .wp-has-current-submenu.opensub .wp-submenu li.current a:focus, #rightnow a:hover, #media-upload a.del-link:hover, div.dashboard-widget-submit input:hover, .subsubsub a:hover, .subsubsub a.current:hover, .ui-tabs-nav a:hover, #wpadminbar .ab-top-menu > li:hover > .ab-item, #wpadminbar .ab-top-menu > li.hover > .ab-item, #wpadminbar .ab-top-menu > li > .ab-item:focus, #wpadminbar.nojq .quicklinks .ab-top-menu > li > .ab-item:focus, #wpadminbar-nojs .ab-top-menu > li.menupop:hover > .ab-item, #wpadminbar .ab-top-menu > li.menupop.hover > .ab-item, #wpadminbar .quicklinks .menupop ul li a:hover, #wpadminbar .quicklinks .menupop ul li a:focus, #wpadminbar .quicklinks .menupop ul li a:hover strong, #wpadminbar .quicklinks .menupop ul li a:focus strong, #wpadminbar .quicklinks .menupop.hover ul li a:hover, #wpadminbar .quicklinks .menupop.hover ul li a:focus, #wpadminbar.nojs .quicklinks .menupop:hover ul li a:hover, #wpadminbar.nojs .quicklinks .menupop:hover ul li a:focus, #wpadminbar li:hover .ab-icon:before, #wpadminbar li:hover .ab-item:before, #wpadminbar li a:focus .ab-icon:before, #wpadminbar li .ab-item:focus:before, #wpadminbar li.hover .ab-icon:before, #wpadminbar li.hover .ab-item:before, #wpadminbar li:hover .ab-item:after, #wpadminbar li.hover .ab-item:after, #wpadminbar li:hover #adminbarsearch:before, #wpadminbar > #wp-toolbar li:hover span.ab-label, #wpadminbar > #wp-toolbar li.hover span.ab-label, #wpadminbar > #wp-toolbar a:focus span.ab-label, #wpadminbar .ab-top-menu>li.hover>.ab-item, #wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus, #wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item, #wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus, #wpadminbar:not(.mobile)>#wp-toolbar a:focus span.ab-label, #wpadminbar:not(.mobile)>#wp-toolbar li:hover span.ab-label, #wpadminbar>#wp-toolbar li.hover span.ab-label{color: #4db848;}
	.acf-button.blue{
		background: #4db848;
		border-color: #3d9439;
		color: white;
		-webkit-box-shadow: inset 0 1px 0 #83cd7f, 0 1px 0 rgba(0, 0, 0, .15);
		box-shadow: inset 0 1px 0 #83cd7f, 0 1px 0 rgba(0, 0, 0, .15);	
	}
	.acf-button.blue:hover{
		background: #409b3c;
		border-color: #368132;
		color: white;
		-webkit-box-shadow: inset 0 1px 0 #71c66d, 0 1px 0 rgba(0, 0, 0, .15);
		box-shadow: inset 0 1px 0 #71c66d, 0 1px 0 rgba(0, 0, 0, .15);
	}
	</style>';
	if(!is_op()):
		echo '<style type="text/css">#toplevel_page_aiowpsec{display: none !important;}</style>';
	endif;
}
add_action('admin_head', 'custom_admin_styles');

// Reklame link i bunden til OP i stedet for til WP
function custom_admin_footer () {
    echo '<span id="footer-thankyou">Udviklet af <a href="http://www.onlineplus.dk" target="_blank">OnlinePlus</a></span>';
}
add_filter('admin_footer_text', 'custom_admin_footer');

// Custom login side
function custom_login_styles(){
    echo '<style type="text/css">
	body{background:url(' . get_bloginfo('template_directory') . '/admin/images/login_bg.jpg) no-repeat center center fixed;} @media screen and (min-width: 2000px){body{background-size:cover;}}
	a{color: #378a00;} a:hover{color: #4db848;}
	h1 a {  background-image:url(' . get_bloginfo('template_directory') . '/admin/images/admin_logo.png) !important; width: auto !important; background-size: auto !important; }
	div.updated, .login .message {background-color: #EBFFEB !important;border-color: #4db848 !important;}
	#login_error, .login .message { margin: 0 auto 16px !important; }
	.login p#nav a, .login p#backtoblog a { color: #378a00 !important; }
	.login p#nav a:hover, .login p#backtoblog a:hover { color: #4db848 !important; text-decoration: none !important; }
	#backtoblog { margin: 5px 0 0; }
	.login a:focus, .login a:active{ outline: 0 !important; }
	#login{ padding-top: 10% !important; max-width: 100% !important; }
	.login form{ margin-left: 0 !important; border-color: #EFEFEF !important; }
	.wp-core-ui .button-primary {background-color: #3d9800 !important; background-image: -webkit-gradient(linear, left top, left bottom, from(#2ac52e), to(#3d9800)) !important; background-image: -webkit-linear-gradient(top, #2ac52e, #3d9800) !important; background-image: -moz-linear-gradient(top, #2ac52e, #3d9800) !important; background-image: -ms-linear-gradient(top, #2ac52e, #3d9800) !important; background-image: -o-linear-gradient(top, #2ac52e, #3d9800) !important;	background-image: linear-gradient(to bottom, #2ac52e, #3d9800) !important; border-color: #3d9800 !important; border-bottom-color: #288d1e !important; -webkit-box-shadow: inset 0 1px 0 rgba(140,230,120,0.5) !important;	box-shadow: inset 0 1px 0 rgba(140,230,120,0.5) !important; }
	.wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:hover, .wp-core-ui .button-primary.focus, .wp-core-ui .button-primary:focus { background-color: #27b749 !important;background-image: -webkit-gradient(linear, left top, left bottom, from(#2ed249), to(#3d9800)) !important;background-image: -webkit-linear-gradient(top, #2ed249, #3d9800) !important;background-image: -moz-linear-gradient(top, #2ed249, #3d9800) !important;background-image: -ms-linear-gradient(top, #2ed249, #3d9800) !important;background-image: -o-linear-gradient(top, #2ed249, #3d9800) !important;background-image: linear-gradient(to bottom, #2ed249, #3d9800) !important;border-color: #1b7f22 !important;-webkit-box-shadow: inset 0 1px 0 rgba(140,230,120,0.6) !important;box-shadow: inset 0 1px 0 rgba(140,230,120,0.6) !important;}
	</style>';
}
add_action('login_head', 'custom_login_styles');
function custom_login_url(){
	return 'http://onlineplus.dk';
}
add_filter( 'login_headerurl', 'custom_login_url' );
function custom_url_title(){
	return 'OnlinePlus - Professionelle Webløsninger Siden 1997';
}
add_filter( 'login_headertitle', 'custom_url_title' );