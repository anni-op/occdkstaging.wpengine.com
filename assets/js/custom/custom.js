"undefined" == typeof Close_Mobile_Menu && (Close_Mobile_Menu = "Luk menu");
$(document).ready(function () {

	if($('.cheat-fix').length){
		//console.log('testing');
		$( window ).resize(function() {
			var header_height = $('#header').height();
			$('.cheat-fix').css('height', header_height);
		}).resize();
	}

	if($('#search_js').length > 0){

        $('#search_js').on('click', function(){
			$('.search_field').toggle()
			$(this).find('.search_btn > .fa').toggleClass('fa-search').toggleClass('fa-times');
			$(this).find('#h_search').focus();
            
		});

		$('.search_field').on('click', function(e){
			//e.preventDefault();
			e.stopPropagation();
		});

		$('.submit_search_btn').on('click', function(e){
			
			$(this).closest('#search_form').submit();
			
		});
		
	}

	$('.search_mobile').on('click', function(){
		$('.search_field').toggle()
		$(this).find('.search_btn > .fa').toggleClass('fa-search').toggleClass('fa-times');
		$(this).find('#h_search').focus();
	})
	
	$('.usp_links_kasse_item .usp_kasse').matchHeight();
	$('.nyheds-item .bg').matchHeight();


	//Galleri_slider
	var galleri_slider = $('.galleri_slider');
    if (galleri_slider.length > 0) {

    $(galleri_slider).each(function () {

      var slickInduvidual = $(this);
      
      slickInduvidual.slick({
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 6000,
        fade: true, // false = slide
        cssEase: 'ease-in',
        infinite: true,
        pauseOnHover: true,        
        autoHeight: false,
        prevArrow: '<span class="slick-prev"><i class="far fa-long-arrow-alt-left"></i></span>',
        nextArrow: '<span class="slick-next"><i class="far fa-long-arrow-alt-right"></i></span>',
        appendArrows:  $(this).siblings('.op_slider_arror_wrapper').find('.op_slider_arror'),
        mobileFirst: true,	
        
      })
	})
	}


	//Citat_slider
	var citat_slider = $('.citat_slider');
    if (citat_slider.length > 0) {

    $(citat_slider).each(function () {

      var slickInduvidual = $(this);
      
      slickInduvidual.slick({
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 9000,
        fade: true, // false = slide
        cssEase: 'ease-in',
        infinite: true,
        pauseOnHover: true,        
        autoHeight: false,
        prevArrow: '<span class="slick-prev"><i class="far fa-long-arrow-alt-left"></i></span>',
        nextArrow: '<span class="slick-next"><i class="far fa-long-arrow-alt-right"></i></span>',
        appendArrows:  $(this).siblings('.op_slider_arror_wrapper').find('.op_slider_arror'),
        mobileFirst: true,	
        
      })
	})
	}

	var tabs_mixer = $('.tabs_content .container');
    if (tabs_mixer.length > 0) {

		if(document.location.hash){
			$filter = '.'+document.location.hash.replace('#', '');
		}
		else{
			$filter = '.tab_0';
		}
		

		var mixer = mixitup(tabs_mixer, {
			selectors: {
				target: '.tab_item',
			},
			animation: {
				duration: 300
			},
			load: {
				filter: $filter //'.tab_0'
			}
		});
	}

	
	if ($('.tabs_button_wrapper').length) {
		
		$('.tabs_button_wrapper .button').on('click', function (e) {
			$indholds_id = $(this).data('filter').replace('.', '');
			
			window.history.pushState("string", "Title", window.location.href.split('#')[0]+ '#' + $indholds_id);
			// = window.location.href.split('#')[0] + '#' + $indholds_id;
			
			//console.log('1: ' + $indholds_id);
		})
	}



	//events mixitup
	$('.event_item .bg').matchHeight();

	var event_mixer = $('.event_mixer');
    if (event_mixer.length > 0) {

		if(document.URL.split('#')[1]){
			var filter_by = '.'+document.URL.split('#')[1];		

		}else{
			var filter_by = 'all';
		}

		console.log('->'+filter_by);
		

		var mixer = mixitup(event_mixer, {
			selectors: {
				target: '.event_item'
			},
			animation: {
				duration: 300
			},
			load: {
				filter: filter_by //'all'
			},
			callbacks:{
				onMixFail: function(state){
					mixItUp(event_mixer).filter('all');
				}
			}
		});

		$('.event_sortering .button').on('click', function(){
			var data_filter = $(this).data('filter').replace('.', '');
			window.history.pushState('string', 'Title', window.location.href.split('#')[0]+'#'+data_filter);
			console.log(data_filter);
		})
	}

	
	$('.read_more_btn').on('click', function(){
	
		$element = $(this).parent('.read_more').toggleClass('expand');
		if (  $element.hasClass('expand') ) {
			//console.log('add: ');
			$text = $(this).data('less');
			$(this).html($text);
		  } else {
			//console.log('remove: ');
			$text = $(this).data('more');
			$(this).html($text);
		  }
		
	})

//	Mobil menu
	var $menu = $("#navbar").clone();
	$menu.find(".dropdown > .container > li:first-child").unwrap();
	$menu.children('ul').append('<li><a href="#outer-container" id="Close_Mobile_Menu">' + Close_Mobile_Menu + '</a></li>');
	$menu.attr("id", "Mobile_Menu");
	$menu.find("span.sub").prepend('<span class="hide"> - </span>');
	$menu.mmenu({
		extensions: ["pageshadow"],
		counters: true,
		iconPanels: {
			add: true,
			visible: 2
		},
		navbars: {
			title: "Menu",
			add: false
		}
	}, {
		clone: true
	});

	$('#mm-Mobile_Menu .mm-title').html($('#logo').clone().attr('id', 'mm-logo'));
	var API = $("#Mobile_Menu").data("mmenu");
	$("#Toggle_Mobile_Menu").click(function () {
		API.open();
	});

	//	Galleri
	var gallery_counter = 0;
	$(".simple_gallery").each(function () {
		$(this).paginate({perPage: 12, class: "gallery_page" + gallery_counter, position: "after"});
		gallery_counter++;
	});

//	Tilføj korrekte klasser på kategori menu
	if (typeof active_categories !== 'undefined') {
		var cat_array = active_categories.split(',');
		for (var i = 0; i < cat_array.length; i++) {
			var active_cat = cat_array[i];
			$("#category_tree_menu li.cat-item-" + active_cat).addClass("current-cat");
		}
	}
	$("#category_tree_menu li.current-cat").parentsUntil("#category_tree_menu").addClass("current-cat-parent");

//	Slider
	if ($('#slider').length > 0) {
		$('#slider').on('init', function (slick) {
			$(".slick-dots").wrap('<div class="container absolute dots_wrap"></div>');
			$(".slick-current > .caption_wrap .caption").fadeIn(500);
		});
		$('#slider').slick({
			dots: false,
			arrows: true,
			autoplay: true,
			autoplaySpeed: 6000,
			fade: true, // false = slide
			cssEase: 'ease-in',
			infinite: true,
			pauseOnHover: true,
			pauseOnDotsHover: true,
			speed: 500,
			autoHeight: true,
			vertical: false,
			prevArrow: '<span class="slick-prev"><i class="fa fa-angle-left"></i></span>',
			nextArrow: '<span class="slick-next"><i class="fa fa-angle-right"></i></span>'
		});
	}

//	Instafeed
	if ($('#instaFeed').length > 0) {
		$('#instaFeed').slick({
			slidesToShow: 10,
			autoplay: true,
			pauseOnHover: true,
			arrows: false,
			autoplaySpeed: 3000,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 1830,
					settings: {
						slidesToShow: 8
					}
				},
				{
					breakpoint: 1460,
					settings: {
						slidesToShow: 6
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 5
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 568,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});
	}
});

$(window).load(function () {
//	Parallax effekt - banner
	$("#banner").length > 0 && $(window).scroll(function () {
		var n = $("#banner").offset().top, o = $(window).scrollTop();
		o > n ? $("#banner > img").css("top", (o - n) / 3 + "px") : $("#banner > img").css("top", "0");
	});
//	Parallax effekt - slider - kan lagge en smule
//	$("#slider").length>0&&$(window).scroll(function(){var s=$("#slider").offset().top,i=$(window).scrollTop();i>s?$("#slider .slick-track > .slick-slide > img").css("top",(i-s)/3+"px"):$("#slider .slick-track > .slick-slide > img").css("top","0")});
});

//Moduler

//Avanceret galleri - kan slettes hvis det ikke bruges
$(window).load(function () {
	$(".advanced_gallery").length > 0 && $(".advanced_gallery").mixItUp({selectors: {target: ".adv_gal"}, layout: {display: "block"}, callbacks: {onMixLoad: function () {
				$("#content").removeClass("loading")
			}}})
});

//Medarbejdere - kan slettes hvis det ikke bruges
$(window).load(function () {
	$(".employees_section").length > 0 && $(".employees_section").mixItUp({selectors: {target: ".employee"}, layout: {display: "block"}, callbacks: {onMixLoad: function () {
				$("#content").removeClass("loading")
			}}})
});

//Referencer - kan slettes hvis det ikke bruges
$(window).load(function () {
	if ($(".references_section").length > 0) {
		$(".references_section").mixItUp({selectors: {target: ".reference"}, layout: {display: "block"}})
	}
});

//Popup besked - kan slettes hvis det ikke bruges
function setPopupCookie(e, o, t) {
	var i = new Date;
	i.setTime(i.getTime() + 24 * t * 60 * 60 * 1e3);
	var n = "expires=" + i.toUTCString();
	document.cookie = e + "=" + o + "; " + n
}
function getPopupCookie(t) {
	for (var n = t + "=", r = document.cookie.split(";"), e = 0; e < r.length; e++) {
		for (var o = r[e]; " " == o.charAt(0); )
			o = o.substring(1);
		if (0 == o.indexOf(n))
			return o.substring(n.length, o.length)
	}
	return""
}
$(window).load(function () {
	$("#abs_popup").removeClass("initial"), $initial_popup_state = $("#abs_popup").attr("class");
	var p = getPopupCookie("popup_state"), s = $("#abs_popup").outerHeight();
	"" != p && "undefined" !== p && null != p || p ? ($("#abs_popup").addClass(p), "closed" == p && $("#abs_popup").css("bottom", "-" + s + "px")) : ("closed" == $initial_popup_state && $("#abs_popup").css("bottom", "-" + s + "px"), setPopupCookie("popup_state", $initial_popup_state, 1)), $("#abs_popup > .handle").click(function () {
		$("#abs_popup").hasClass("open") ? ($("#abs_popup > .handle i").toggleClass("fa-angle-up fa-angle-down"), $("#abs_popup").animate({bottom: -s}, 300, function () {
			$("#abs_popup").toggleClass("open closed"), setPopupCookie("popup_state", "closed", 1)
		})) : ($("#abs_popup > .handle i").toggleClass("fa-angle-up fa-angle-down"), $("#abs_popup").animate({bottom: 0}, 300, function () {
			$("#abs_popup").toggleClass("open closed"), setPopupCookie("popup_state", "open", 1)
		}))
	}), $("#abs_popup_side").removeClass("initial"), $initial_popup_side_state = $("#abs_popup_side").attr("class");
	var o = getPopupCookie("popup_side_state"), e = $("#abs_popup_side").outerWidth();
	"" != o && "undefined" !== o && null != o || o ? ($("#abs_popup_side").addClass(o), "closed" == o && $("#abs_popup_side").css("right", "-" + e + "px")) : ("closed" == $initial_popup_side_state && $("#abs_popup_side").css("right", "-" + e + "px"), setPopupCookie("popup_side_state", $initial_popup_side_state, 1)), $("#abs_popup_side > .handle").click(function () {
		$("#abs_popup_side").hasClass("open") ? ($("#abs_popup_side > .handle i").toggleClass("fa-angle-left fa-angle-right"), $("#abs_popup_side").animate({right: -e}, 300, function () {
			$("#abs_popup_side").toggleClass("open closed"), setPopupCookie("popup_side_state", "closed", 1)
		})) : ($("#abs_popup_side > .handle i").toggleClass("fa-angle-left fa-angle-right"), $("#abs_popup_side").animate({right: 0}, 300, function () {
			$("#abs_popup_side").toggleClass("open closed"), setPopupCookie("popup_side_state", "open", 1)
		}))
	})
});

//Cookie besked
function getCookie(e) {
	var i = document.cookie, o = i.indexOf(" " + e + "=");
	if (-1 == o && (o = i.indexOf(e + "=")), -1 == o)
		i = null;
	else {
		o = i.indexOf("=", o) + 1;
		var n = i.indexOf(";", o);
		-1 == n && (n = i.length), i = unescape(i.substring(o, n))
	}
	return i
}
function setCookie(e, i, o) {
	if (o) {
		var n = new Date;
		n.setTime(n.getTime() + 24 * o * 60 * 60 * 1e3);
		var t = "; expires=" + n.toGMTString()
	} else
		var t = "";
	document.cookie = e + "=" + i + t + "; path=/"
}
function close_cookie() {
	setCookie("cookie_accept", "true", 30), $("#zc_cookie_notice").slideUp(300, function () {
		$(this).remove()
	})
}
