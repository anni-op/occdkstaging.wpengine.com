<?php get_header(); ?>

<section class="page_header">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 h1 bigger">
				<?php  yoast_breadcrumb('<p id="breadcrumbs" >','</p>'); ?>
				<?php the_title();?>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('modules/acf', 'sektioner'); ?>

<?php if(get_the_content()): ?>
<section id="content" <?php post_class( 'page' ); ?>>
    <div class="container">
		<div class="row">
            <div class="col-xs-12">
				<?php
				wp_reset_query();
				if (have_posts()) : while (have_posts()) : the_post();
						the_content();
					endwhile;
				endif;
				?>
				
            </div>
		</div>

    </div>
</section>
			<?php endif; ?>

<?php 
$show = get_field('vis_global_cta');
if($show === NULL || $show):
	echo '<section class="global_cta"></section>';
	get_template_part('modules/sektioner/cta'); 
else:
	
endif;
?>

<?php
get_footer();
