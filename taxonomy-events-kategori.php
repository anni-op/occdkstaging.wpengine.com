<?php get_header(); ?>
	
<section id="content" class="archive">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><?php _e(single_cat_title( '', false ), 'onlineplus-general'); ?></h1>
            </div>
         
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
                get_template_part('loop','event');
endwhile;
endif;
            ?>
          
        </div>
    </div>
</section>

<?php get_footer();