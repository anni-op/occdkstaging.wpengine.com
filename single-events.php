<?php get_header(); ?>
	
<section id="content" <?php post_class(); ?>>
    <div class="container">
        <div class="row">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
				<article >
					<?php 
                    if(has_post_thumbnail()):
						$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'slideshow');   
						echo '<div class="col-xs-12 text-center event_image">';                   
							echo '<img src="'.$thumbnail[0].'" alt="" class="" />';
						echo '</div>';
                    endif;?>

				<div class="col-xs-12 col-sm-8">
					<h1><?php the_title(); ?></h1>
					
					<?php the_content(); ?>
				</div>

				<div class="col-xs-12 col-sm-4">
					<div class="event_box">

							<?php $billet_link = get_field('billet_link');; 
							if($billet_link ): ?>
							<div>
								
								<?php			
									echo '<a class="button" href='.$billet_link['url'].' target='.($billet_link['target'] ? $billet_link['target'] : '_self' ).' >'.($billet_link['title'] ? $billet_link['title'] : 'Læs mere' ).' <i class="fas fa-long-arrow-right"></i></a>';
								
								?>
							</div>
							<?php endif;?>

							<?php $pris = get_field('pris'); 
							if($pris ): ?>
								<div>
									<strong><?php echo __('Pris', 'onlineplus-general');?>:</strong> 
									<?php echo $pris; ?>
								</div>
							<?php endif;?>

						
							<?php 
							$dato = get_field('dato'); 
							$s_dato = get_field('slut_dato');
							if($dato ):?>
								<div>
									<strong><?php echo __('Dato', 'onlineplus-general');?>:</strong> 
									<?php echo $dato; ?>

									<?php
										if($s_dato != $dato && $s_dato):
											echo '- '. $s_dato;
										endif;
									?>
								</div>

							<?php endif;?>
						
						
							<?php $tidspunkt = get_field('tidspunkt'); 
							if($tidspunkt ): ?>
								<div>
									<strong><?php echo __('Tidspunkt', 'onlineplus-general');?>:</strong> 
									<?php echo $tidspunkt; ?>
								</div>
							<?php endif;?>
						
						
						<?php $link = get_field('sted');; 
							if($link ): ?>
							<div>
								<strong><?php echo __('Sted', 'onlineplus-general');?>:</strong> 
								<?php							
									echo '<a class="" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.($link['title'] ? $link['title'] : 'Læs mere' ).'</a>';
								?>
							</div>
							<?php endif;?>
							
						
						
						<?php $indgang = get_field('indgang'); 
							if($indgang ): ?>
							<div>
								<strong><?php echo __('Indgang', 'onlineplus-general');?>:</strong> 
								<?php echo $indgang; ?>
							</div>
							<?php endif; ?>
						
						
							<?php $type = get_field('type'); 
							if($type ): ?>
							<div>
								<strong><?php echo __('Type', 'onlineplus-general');?>:</strong> 							
								<?php echo $type; ?>
							</div>
							<?php endif; ?>
						
					</div>
				</div>

				</article>
            <?php endwhile; endif; ?>
		</div>
    </div>
</section>

<?php get_footer();