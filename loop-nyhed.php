<?php 

echo '<div class="'.$container_col.' nyheds-item">';
    echo '<a href="'.get_the_permalink().'">';
        if(has_post_thumbnail()):
            $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'nyheder');   
            echo '<div class="event_image">';                   
                echo '<img src="'.$thumbnail[0].'" alt="" class="" />';
            echo '</div>';
        endif;
        echo '<div class="bg">';
            echo  '<div class="overskrift"><strong>'.get_the_title().'</strong></div>';
            echo '<div class="dato">'.get_the_date().'</div>';

        echo '</div>';
    echo '</a>';
echo '</div>';


?>