<?php
/*
 * HUSKELISTE:
 * - Minify CSS, JS og billeder (https://tinypng.com)
 * - Saml scripts i så få filer som det er muligt
 * - Ved nye "post types" og/eller "taxonomies" brug da vores post type modul, ikke registrer dem direkte i temaet
 */
header('X-Frame-Options: SAMEORIGIN');
ini_set("expose_php", 0);
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('template_redirect', 'rest_output_link_header', 11, 0);

//WP Memory limit i MB
define('ABS_MEMORY_LIMIT', '64');
define('OPFW_VERSION', '6.1');

//Aktiver facebook scripts (til like-knap, væg og lign.)
define('FACEBOOK_SCRIPTS', false);

include_once(TEMPLATEPATH . '/assets/template-v-'.OPFW_VERSION.'/includes/modules.php');

//Enqueue Scripts and Styles
$template_css_uri = get_template_directory_uri() . '/assets/template-v-'.OPFW_VERSION.'/css';
$template_js_uri = get_template_directory_uri() . '/assets/template-v-'.OPFW_VERSION.'/js';

function op_enqueues()
{
    global $template_css_uri, $template_js_uri;
    $stylesheet_uri = get_stylesheet_directory_uri();

    $themecsspath = get_stylesheet_directory() . '/assets/css/custom.css';
    $style_ver = filemtime($themecsspath);
    
    wp_enqueue_style('op-custom', $stylesheet_uri . '/assets/css/custom.css', array(), $style_ver);
    wp_enqueue_style('op-font-awesome', $stylesheet_uri .'/assets/fontawesome/css/all.min.css');
    
    wp_deregister_script('jquery'); // Deregister WordPress' jQuery
    wp_register_script('jquery', $template_js_uri . '/jquery-2.2.4.min.js', array(), '2.2.4', true); 
    //	Indeholder mobilmenu, slick slider o.lign.
    wp_enqueue_script('op-scripts', $template_js_uri . '/combined.min.js', array('jquery'), OPFW_VERSION, true);
    if ((defined('AVANCERET_GALLERI_UNDERSIDER') && AVANCERET_GALLERI_UNDERSIDER) || (defined('AVANCERET_GALLERI_FORSIDE') && AVANCERET_GALLERI_FORSIDE) || (defined('SIMPELT_GALLERI_UNDERSIDER') && SIMPELT_GALLERI_UNDERSIDER) || (defined('SIMPELT_GALLERI_FORSIDE') && SIMPELT_GALLERI_FORSIDE) || (defined('INSTAFEED') && INSTAFEED)):
        wp_enqueue_script('op-gallery-scripts', $template_js_uri . '/gallery/js/blueimp.combined.min.js', array('jquery'), '2.21.3', true);
    define('INIT_LIGHTBOX', true);
    endif;
    // if ((defined('AVANCERET_GALLERI_UNDERSIDER') && AVANCERET_GALLERI_UNDERSIDER) || (defined('AVANCERET_GALLERI_FORSIDE') && AVANCERET_GALLERI_FORSIDE)):
    //     wp_enqueue_script('op-mixitup', $template_js_uri . '/jquery.mixitup.min.js', array('jquery'), '2.1.11', true);
    // endif;
    // if ((defined('EMPLOYEES') && EMPLOYEES)):
    //     wp_enqueue_script('op-mixitup', $template_js_uri . '/jquery.mixitup.min.js', array('jquery'), '2.1.11', true);
    // endif;
    // if (defined('REFERENCES') && REFERENCES):
    //     wp_enqueue_script('op-mixitup', $template_js_uri . '/jquery.mixitup.min.js', array('jquery'), '2.1.11', true);
    // endif;

    wp_enqueue_script('op-mixitup', $stylesheet_uri . '/assets/js/mixitup.min.js', array('jquery'), '3.3.0', true);
    wp_enqueue_script('op-match-height', $stylesheet_uri . '/assets/js/jquery.matchHeight-min.js', array('jquery'), '3.3.0', true);

    wp_register_script('op-app', $stylesheet_uri . '/assets/js/custom.min.js', array('jquery'), OPFW_VERSION, true);
    if (defined('RESPONSIVE_MENU_CLOSE')):
        wp_localize_script('op-app', 'Close_Mobile_Menu', RESPONSIVE_MENU_CLOSE);
    endif;
    wp_enqueue_script('op-app');
}
add_action('wp_enqueue_scripts', 'op_enqueues');

function custom_pagination_slug()
{
    global $wp_rewrite;
    //	Lav check for WPML - er endnu ikke understøttet
    if (!defined('ICL_LANGUAGE_CODE')):
        $wp_rewrite->pagination_base = 'side';
    endif;
}
//add_action('init', 'custom_pagination_slug');

// Registrer Options Pages
if (function_exists('acf_add_options_page') && !function_exists('op_register_options_pages')):
    
    function op_register_options_pages()
    {
        global $current_user;
        acf_add_options_page(array(
            'page_title' 	=> __('Global settings', 'onlineplus-general'),
            'menu_title'	=> __('Global settings', 'onlineplus-general'),
            'menu_slug' 	=> 'acf-options',
            'capability'	=> 'edit_posts',
            'redirect'		=> false
        ));
        if ($current_user->user_login == 'OnlinePlus'):
            acf_add_options_page(array(
                'page_title' 	=> 'Moduler & Indstillinger',
                'menu_title'	=> 'Moduler & Indstillinger',
                'menu_slug' 	=> 'acf-modules',
                'capability'	=> 'manage_options',
                'redirect'		=> false
            ));
        endif;


        acf_add_options_sub_page(array(
            'page_title' 	=> 'Events Settings',
            'menu_title' 	=> 'Events Settings',
            'parent_slug' 	=> 'edit.php?post_type=events',
        ));

    }
    add_action('init', 'op_register_options_pages', 1);
    
endif;

// Læs mere på uddrag
function custom_excerpt_more($more)
{
    return '';
//    return '... <a class="read_more" href="' . get_permalink(get_the_ID()) . '">' . __('Læs mere &#10095;', 'onlineplus-general') . '</a>';
}
add_filter('excerpt_more', 'custom_excerpt_more');

function custom_excerpt_length($length)
{
    return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

// Søgeformular
function new_op_search_form($form)
{
    $form = '<form id="search_form" role="search" method="get" class="searchform" action="' . home_url('/') . '" >       
        <input id="h_search" type="search" class="search-field" name="s" placeholder="' . __('Søg efter...', 'onlineplus-general') . '" />
        <i class="fa fa-search submit_search_btn" aria-hidden="true"></i>
		<input type="submit" class="search-submit" value="' . __('Search', 'onlineplus-general') . '" />
    </form>';

    return $form;
}
add_filter('get_search_form', 'new_op_search_form', 99);

function op_enqueue_ig_script($hook)
{
    global $template_css_uri, $template_js_uri;
    $currentScreen = get_current_screen();
    $currentScreen = (substr_count($currentScreen->base, '_')?substr($currentScreen->base, strripos($currentScreen->base, '_')+1):$currentScreen->base);
    if ('admin.php' != $hook && $currentScreen != 'acf-modules'):
        return;
    endif;
    $template_uri = get_template_directory_uri();
    wp_enqueue_script('op-instafeed-admin', $template_js_uri . '/instafeed-admin.min.js', array('jquery'));
    wp_register_style('op-instafeed-admin', $template_css_uri . '/instafeed-admin.min.css');
    wp_enqueue_style('op-instafeed-admin');
}
add_action('admin_enqueue_scripts', 'op_enqueue_ig_script');

//register_nav_menu('main_menu', __('Hovedmenu'));
//add_custom_role('intranet_bruger', 'Intranet bruger');

/*
 * Post type:
 * Brug vores "custom post type" modul/plugin!
 *
 * Taxonomy:
 * Brug vores "custom post type" modul/plugin!
 *
 * WP Query (custom queries):
 * https://generatewp.com/wp_query/
 *
 * Menuer:
 * https://generatewp.com/nav-menus/
 */

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'billedesektion_big', 725 , 487, true ); 
    add_image_size( 'billedesektion_small', 525, 236, true ); 
    add_image_size( 'slideshow', 1265, 500, true );
    add_image_size( 'link_bokse', 740, 422, true );
    add_image_size( 'tabs', 740, 525, true );
    add_image_size( 'square', 740, 750, true );
    add_image_size( 'nyheder', 740, 300, true );
}



function register_my_menus(){
    register_nav_menus(
       array(
           'top-menu'=> __('Top Menu', 'onlineplus-general'),

           'overfooter_1'=> __('Overfooter 1', 'onlineplus-general'),
           'overfooter_2'=> __('Overfooter 2', 'onlineplus-general'),
           'overfooter_3'=> __('Overfooter 3', 'onlineplus-general'),
           'overfooter_4'=> __('Overfooter 4', 'onlineplus-general'),

           'om-menu'=> __('Om Menu', 'onlineplus-general'),
           'kontakt-menu'=> __('Kontakt Menu', 'onlineplus-general'),
           'praktisk-menu'=> __('Praktisk Menu', 'onlineplus-general'),

       )
    );
}
add_action('init', 'register_my_menus');
  
add_filter('wp_nav_menu_items','add_search_box_to_menu', 10, 2);
function add_search_box_to_menu( $items, $args ) {


    if( $args->theme_location == 'main_menu' ){
        $menu = (wp_get_nav_menu_object($args->menu) ? wp_get_nav_menu_object($args->menu) :'false');
        $link = get_field('book_hotel', $menu);  

        return $items."<li id='search_js' class='menu-header-search'>
                            <span  class='search_btn'>
                                <i class='fa fa-search' aria-hidden='true'></i>                        
                            </span>
                            <div class='search_field'><div class='container'><div class='row'><div class='col-xs-12'>".get_search_form(false)."</div></div></div>
                            </div>
                        </li>".
                        ($link ? 
                            "<li class='button-item menu-item menu-item-type-post_type menu-item-object-page'>
                            <a href='".$link['url']."' target='".($link['target'] ? $link['target'] : '_self' )."'>".($link['title'] ? $link['title'] : 'Læs mere' )."</a>
                            </li>"
                            : '')
                            ;
    }
    else{
        return $items;
    }
 
    
}

class mega_menu extends Walker_Nav_Menu {

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
      global $wp_query;
      $indent = ( $depth ) ? str_repeat(" ", $depth) : '';

      $item_id = $item->object_id;
      $tekst_menu = get_field('tekst_menu', $item_id);
  
      $class_names = $value = '';
  
      $classes = empty($item->classes) ? array() : (array) $item->classes;
          if (in_array('menu-item-has-children', $classes)): 
              $classes[] = 'has-dropdown'; 
              $classes[] = 'megamenu';
      endif;

      if( $tekst_menu):
        $classes[] = 'has_text_menu';
      endif;

      if (in_array('current-menu-item', $classes)): $classes[] = 'active';
      endif;
      if ($item->description != ''): $classes[] = 'has-description';
          endif;
  
      $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
      $class_names = ' class="' . esc_attr($class_names) . '"';
  
      $output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';
  
      $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
      $attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
      $attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
       $attributes .=!empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
    
      
        if( $tekst_menu):

           $beskrivelse = get_field('menu_beskrivelse', $item_id);
           $cta = get_field('menu_cta', $item_id);
           $billede = wp_get_attachment_image(get_field('menu_billede', $item_id), 'full' );
           
           $item_output = $args->before;
           if(!$billede):
           $item_output .= '<div class="menu_title">'.$args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after.'</div>';
           endif;
         
           $item_output .= '<div class="text">'.$beskrivelse.' <a '.$attributes.'>'.$cta.'</a>'.'</div>';

           $item_output .= '<div><a '.$attributes.'>'.$billede.'</a>'.'</div>';


        
           $item_output .= $args->after;
        else:
       

            $item_output = $args->before;
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
            if ($item->description != ''):
                $item_output .= '<br /><span class="sub">' . $item->description . '</span>';
            endif;
            $item_output .= '</a>';
            $item_output .= $args->after;

        endif;
  
      $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id);


    }
  
    function start_lvl(&$output, $depth = 0, $args = array()) {
      $output .= '<ul class="dropdown" role="menu">';

         if($depth == 0):
            $output .= '<div class="container">';
        endif;

      }
      
      function end_lvl(&$output, $depth = 0, $args = array()){
          
        if($depth == 0):
            $output .= '</div>';
        endif;

        $output .= '</ul>';
          
      }
  
  }

  function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'assets/css/admin-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );
    // Callback function to insert 'styleselect' into the $buttons array
    function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
    }
    // Register our callback to the appropriate filter
    add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );
    // Callback function to filter the MCE settings
    function my_mce_before_init_insert_formats( $init_array ) {  
    // Define the style_formats array
    $style_formats = array(  
        // Each array child is a format with it's own settings
        array(  
        'title' => 'underoverskrift',  
        'inline' => 'span',  
        'classes' => 'underoverskrift',
        'wrapper' => true,      
        ),
        array(  
        'title' => 'lilla knap',  
        'selector' => 'a',  
        'classes' => 'lilla_knap',
                
        ),
        array(  
        'title' => 'Tabel med streger',  
        'selector' => 'table',  
        'classes' => 'tb_streg',
                
        )
    );  
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  
    
    return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

function my_pre_get_posts( $query ) {
	
	// do not modify queries in the admin
	if( is_admin() ) {return $query;}
    
    if(is_single() ) { return $query;}

	// only modify queries for 'event' post type
	if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'events' ) {
        
        $today = date('Ymd');
        // $meta_query[] = array(
        //         'key'		=> 'dato',
        //         'compare'	=> '>=',
        //         'value'		=> $today,
        //     );

    
        $meta_query[] = array(
            array(
                'relation' => 'OR',
                array(
                'key'     => 'dato',
                'value'   => $today,
                'compare' => '>=',
                ),
                array(
                    'key'     => 'slut_dato',
                    'value'   => $today,
                    'compare' => '>=',
                )
            ),
        );

            
        $query->set('meta_query', $meta_query); 
        
		$query->set('orderby', 'meta_value');	
		$query->set('meta_key', 'dato');	 
        $query->set('order', 'ASC'); 
        
        
    }
    
    if ( ! is_admin() && $query->is_search ) {

        $today = date('Ymd');          
        
        $meta_query =  array(
                'relation' => 'OR',
                array(
                    'key'     => 'dato',
                    'value'   => $today,
                    'compare' => '>=',
                ),
                array(
                    'key'     => 'slut_dato',
                    'value'   => $today,
                    'compare' => '>=',
                ),
				array(
                    'key'     => 'dato',
                    'value'   => '',
                    'compare' => 'NOT EXISTS',
                )
            );
        $query->set( 'meta_query', $meta_query );
    }


	// return
	return $query;

}

add_action('pre_get_posts', 'my_pre_get_posts');


add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
	if (!is_user_logged_in()) {
	  show_admin_bar(false);
	}
}

//formidable 
function get_menus(){
    $results = array();
    $menus = wp_get_nav_menus();
    foreach ( $menus as $menu) {
        $results[$menu->term_id] = $menu->name;
    }
    return $results;
}
 
function load_menus_function( $field ){
	$result = get_menus();
	if( is_array($result) ){
		$field['choices'] = array();
		foreach( $result as $key=>$match ){
			$field['choices'][ $key ] = $match;
		}
	}
    return $field;
}
add_filter('acf/load_field/name=op_menu_list', 'load_menus_function'); 

add_filter('get_ancestor_menu', 'get_ancestor_menu');
function get_ancestor_menu($ID){
    global $post;

    $parent = '';
    if ($post->post_parent):
        $ancestors=get_post_ancestors($post->ID);
        //pre($ancestors);

        foreach($ancestors as $pageID):

            $vis_overfooter_menu = get_field('vis_overfooter_menu', $pageID);
            if($vis_overfooter_menu == 'unik'):
                $parent = $pageID;
                break;
            elseif($vis_overfooter_menu == 'hide'):
                $parent = 'hide';
                break;
            endif;

        endforeach;

        if(empty($parent)):
            $parent ='hide';
        endif;

    else:
        $parent = $post->ID;
    endif;

    return $parent;

}
