<?php if(!defined('REFERENCES') || (defined('REFERENCES') && !REFERENCES)): header("HTTP/1.0 404 Not Found"); get_template_part('404'); die(); endif; ?>
<?php get_header(); ?>
	
<section id="content" class="archive">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><?php _e('References', 'onlineplus-general'); ?></h1>
		
		<?php 
                $terms = get_terms('references_categories', array('hide_empty' => false));
		echo '<nav class="choose_reference_cats"><a href="#" class="choose_reference_cat filter active" data-filter="all">'.__('Alle', 'onlineplus-general').'</a>';
		foreach($terms as $cat):
			echo '&emsp;/&emsp;<a href="#" class="choose_reference_cat filter" data-filter=".'.$cat->slug.'">'.$cat->name.'</a>';
		endforeach;
		echo '</nav>'; ?>
                
		<?php m('references'); ?>
            </div>
        </div>		
    </div>
</section>

<?php get_footer();