<?php if(!defined('EMPLOYEES') || (defined('EMPLOYEES') && !EMPLOYEES)): header("HTTP/1.0 404 Not Found"); get_template_part('404'); die(); endif; ?>
<?php get_header(); ?>
	
<section id="content" class="archive">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><?php _e('Employees', 'onlineplus-general'); ?></h1>

                <?php $terms = get_terms('employees_categories', array('hide_empty' => false));
                echo '<nav class="choose_employee_cats"><a href="#" class="choose_employee_cat filter active" data-filter="all">'.__('Alle', 'onlineplus-general').'</a>';
                foreach($terms as $cat):
                        echo '&emsp;/&emsp;<a href="#" class="choose_gallery_cat filter" data-filter=".'.$cat->slug.'">'.$cat->name.'</a>';
                endforeach;
                echo '</nav>'; ?>
                <?php m('employees'); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer();