<?php 

global $post;
if ($post && $post->post_parent)	{
	$ancestors=get_post_ancestors($post->ID);
	$root=count($ancestors)-1;
	$parent = $ancestors[$root];
} else {
	$parent = get_the_ID(); //$post->ID;
}

$blue_pages = array(8273, 8314, 8313, 8312, 8311, 8310, 8309, 8308, 8306); 
if($parent == 44 && !in_array(get_the_ID(), $blue_pages )):  //Møder og konferencer ?>
	<section class="overfooter 123">
	
		 <div class='container'>
			 <div class='row'>
				 <div class="col-xs-12 h1">
				 	<?php the_field('overfooter_overskrift', 'option'); ?>
				 </div>
				 <div class="col-xs-6 col-sm-3 overfooter_menu">
				 	<?php
					 $location_name = 'overfooter_1';
					 $theme_locations = get_nav_menu_locations();
					 $menu_obj = get_term( $theme_locations[$location_name], 'nav_menu' );
					 if(isset($menu_obj->name)):
						 echo '<div class="h3">'.$menu_obj->name.'</div>';
					 endif;
					 wp_nav_menu( array( 'theme_location' => $location_name, 'fallback_cb' => false ) );
					 ?>
				 </div>
				 <div class="col-xs-6 col-sm-3 overfooter_menu">
				 	<?php
					 $location_name = 'overfooter_2';
					 $theme_locations = get_nav_menu_locations();
					 $menu_obj = get_term( $theme_locations[$location_name], 'nav_menu' );
					 if(isset($menu_obj->name)):
						 echo '<div class="h3">'.$menu_obj->name.'</div>';
					 endif;
					 wp_nav_menu( array( 'theme_location' => $location_name, 'fallback_cb' => false ) );
					 ?>
				 </div>
				 <div class="col-xs-6 col-sm-3 overfooter_menu">
				 	<?php
					 $location_name = 'overfooter_3';
					 $theme_locations = get_nav_menu_locations();
					 $menu_obj = get_term( $theme_locations[$location_name], 'nav_menu' );
					 if(isset($menu_obj->name)):
						 echo '<div class="h3">'.$menu_obj->name.'</div>';
					 endif;
					 wp_nav_menu( array( 'theme_location' => $location_name, 'fallback_cb' => false ) );
					 ?>
				 </div>
				 <div class="col-xs-6 col-sm-3 overfooter_menu">
				 	<?php
					 $location_name = 'overfooter_4';
					 $theme_locations = get_nav_menu_locations();
					 $menu_obj = get_term( $theme_locations[$location_name], 'nav_menu' );
					 if(isset($menu_obj->name)):
						 echo '<div class="h3">'.$menu_obj->name.'</div>';
					 endif;
					 wp_nav_menu( array( 'theme_location' => $location_name, 'fallback_cb' => false ) );
					 ?>
				 </div>

				 



			 </div>
		 </div>
	 </section>

<?php endif; ?>