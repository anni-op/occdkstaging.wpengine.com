<?php get_header(); ?>

<?php get_template_part('modules/acf', 'sektioner'); ?>

<section id="content" class="home">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-9">
				<?php
				wp_reset_query();
				if (have_posts()) : while (have_posts()) : the_post();
						the_content();
					endwhile;
				endif;
				?>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<?php m('sidebar'); ?>
			</div>
		</div>
	</div>
</section>

	<?php // get_search_form(); ?>
	<?php // m('google_map'); ?>
	<?php // m('topmenu'); ?>
	<?php // m('custom_menu', array('menu_name' => 'main_menu', 'exclude' => '1,2')); // Custom menu fra "Udseende - Menuer" - Kun ét niveau ?>
	<?php // m('custom_menu', array('parent' => 82, 'multilvl' => false)); // Custom menu udtræk fra side-struktur  ?>
	<?php // m('slider', array('slider_class' => 'container', 'caption_class' => 'container', 'is_mobile' => false, 'type' => 'default')); // type: default/full_link - lazy_load sættes til false v. slide effekt ?>
	<?php // m('quicklinks', array('count' => 4, 'type' => 'full_link')); // almindelig type - default ?>

	<?php // m('instafeed', array('limit' => 33, 'link_to' => 'gallery', 'speed' => 1, 'mode' => 'scroller', 'img_style' => 'default')); ?>
	<?php // m('instafeed', array('limit' => 33, 'link_to' => 'instagram', 'speed' => 1, 'mode' => 'scroller', 'img_style' => 'default'));  ?>
	<?php // m('instafeed', array('limit' => 33, 'link_to' => 'gallery', 'mode' => 'gallery', 'per_row' => 6, 'per_page' => 12, 'pagination_position' => 'after', 'img_style' => 'thumbnail')); ?>
	<?php // m('instafeed', array('limit' => 33, 'link_to' => 'instagram', 'mode' => 'gallery', 'per_row' => 6, 'per_page' => 12, 'pagination_position' => 'after', 'img_style' => 'thumbnail')); ?>

	<?php // m('contact_box', 'sidebar'); ?>
	<?php // m('gallery', array('per_row' => 6, 'new_gallery' => true)); ?>
	<?php // m('gallery_advanced', array('per_row' => 6, 'new_gallery' => true)); ?>
	<?php // m('banner', array('banner_type' => 'full_width')); ?>
	<?php // m('news_archive'); ?>
	<?php // m('news_frontpage', array('category_slug' => 'nyheder', 'posts_per_page' => 2, 'module_headline' => __('Latest news', 'onlineplus-general'), 'archive_link_text' => __('News archive', 'onlineplus-general'))); ?>
	<?php // m('events_archive'); ?>
	<?php // m('events_frontpage', array('posts_per_page' => 2, 'module_headline' => __('Upcoming events', 'onlineplus-general'), 'archive_link_text' => __('All events', 'onlineplus-general'))); ?>
	<?php // m('sidebar'); ?>
	<?php // m('tree_menu', array('children_of_current' => false)); //Husk at sæt $facebook_scripts = true i functions.php ?>
	<?php // m('fb_posts', array('width' => '500', 'height' => '400', 'hide_cover' => false, 'small_header' => false)); //Husk at sæt $facebook_scripts = true i functions.php ?>
	<?php // m('fb_like', array('layout' => 'button_count', 'share' => false)); ?>
	<?php // m('popup');  ?>
	<?php // m('popup', array('location' => 'side'));  ?>
	<?php // m('firma_information', array('overskrift' => 'Kontakt')); ?>
<?php
get_footer();
