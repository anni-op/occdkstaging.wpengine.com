<?php //if(!defined('NEWS') || (defined('NEWS') && !NEWS)): header("HTTP/1.0 404 Not Found"); get_template_part('404'); die(); endif; ?>
<?php get_header(); ?>
	
<section id="content" class="archive">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 archive_indledning">
                <?php
                    $page_for_posts_id = get_option( 'page_for_posts' );
                    echo '<h1>'.get_the_title($page_for_posts_id).'</h1>';
                    echo get_the_content('','', $page_for_posts_id);            				
				?>                
            </div>
        </div>

        <div class="row">
        <?php 

            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $the_query = new WP_Query( array(  'post_type' => 'post',  'post_status' => 'publish', 'paged' => $paged)); //'posts_per_page' => '9', 

            if ( $the_query->have_posts() ) :
                $container_col = 'col-xs-12 col-sm-4';
                set_query_var( 'container_col', $container_col );
                while ( $the_query->have_posts() ) : $the_query->the_post();
                   get_template_part('loop', 'nyhed');
                endwhile;
             
             
                echo '<div class="col-xs-12 pagination_wrapper">';
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    // 'total'        => $the_query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => sprintf( '<i class="fa fa-angle-left"></i> %1$s', __( 'Nyere indlæg', 'onlineplus-general' ) ),
                    'next_text'    => sprintf( '%1$s <i class="fa fa-angle-right"></i>', __( 'Ældre indlæg', 'onlineplus-general') ),
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                echo '</div>'; 

                wp_reset_postdata();

            endif;

            

            ?>
        
        </div>

    </div>
</section>

<?php get_footer();