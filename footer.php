<?php

//old/overfooter_menu.php

$vis_overfooter_menu = get_field( 'vis_overfooter_menu' );
switch ( $vis_overfooter_menu ) {
	case 'unik':
		$id = get_the_ID();
		//echo 'Unik '. $id;
		break;
	case 'hide':
		$id = 'hide';
		//echo 'Hide '. $id;
		break;
	case 'NULL';
	default: //vis forældre
		$id = apply_filters( 'get_ancestor_menu', get_the_ID() );
	//echo 'parent '. $id;
}

$values = get_field( 'op_menu_list', $id );

if ( $id != 'hide' && $values ):
	echo "<section class='overfooter' data-page_id='" . $id . "'>
				<div class='container'>
					<div class='row'>
						<div class='col-xs-12 h1'>";
	the_field( 'footer_menu_overskrift', $id );
	echo '</div>';

	if ( $values ):
		foreach ( $values as $value ) {
			echo '<div class="col-xs-6 col-sm-3 overfooter_menu">';

			$menu_id = $value;

			$menu_obj = wp_get_nav_menu_object( $menu_id );
			if ( isset( $menu_obj->name ) ):
				echo '<div class="h3">' . $menu_obj->name . '</div>';
			endif;
			wp_nav_menu( array( 'menu' => $menu_id, 'fallback_cb' => false ) );

			echo '</div>';
		}
	endif;

	echo '</div>
			</div>
		</section>';
endif;

?>


<footer id="footer">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-3 footer-col text-center v_info">
				<?php

				$logo = get_field( 'logo_hvid', 'option' );
				if ( $logo ):
					echo '<a href="' . get_bloginfo( 'url' ) . '">' . wp_get_attachment_image( $logo, 'full', '',
							array( 'class' => 'footer_logo' ) ) . '</a>';
				else: ?>
                    <a href="<?php bloginfo( 'url' ); ?>" id="logo"><img src="assets/images/logo.png"
                                                                         alt="<?php bloginfo( 'name' ); ?>"></a>
				<?php endif; ?>

				<?php m( 'firma_information', array( 'overskrift' => 'Kontakt' ) ); ?>

                <div id="some" class="">


					<?php if ( have_rows( 'some', 'option' ) ) : ?>

						<?php while ( have_rows( 'some', 'option' ) ) : the_row(); ?>

							<?php
							$ikon = get_sub_field( 'some_ikon' );
							$link = get_sub_field( 'link' );
							if ( $link && $ikon ) {
								echo '<a class="" href=' . $link . ' target="_blank" >' . ( $ikon ? $ikon : '' ) . '</a>';
							}
							?>

						<?php endwhile; ?>

					<?php endif; ?>
                </div>
            </div>
            <div>
                <div class="col-xs-6 col-sm-6 col-md-3 footer-col">
					<?php
					$location_name   = 'om-menu';
					$theme_locations = get_nav_menu_locations();
					$menu_obj        = get_term( $theme_locations[ $location_name ], 'nav_menu' );
					if ( isset( $menu_obj->name ) ):
						echo '<div class=" menu_overskrift">' . $menu_obj->name . '</div>';
					endif;
					wp_nav_menu( array( 'theme_location' => $location_name, 'fallback_cb' => false ) );
					?>

                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 footer-col">
					<?php
					$location_name   = 'kontakt-menu';
					$theme_locations = get_nav_menu_locations();
					$menu_obj        = get_term( $theme_locations[ $location_name ], 'nav_menu' );
					if ( isset( $menu_obj->name ) ):
						echo '<div class=" menu_overskrift">' . $menu_obj->name . '</div>';
					endif;
					wp_nav_menu( array( 'theme_location' => $location_name, 'fallback_cb' => false ) );
					?>

                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 footer-col">
					<?php
					$location_name   = 'praktisk-menu';
					$theme_locations = get_nav_menu_locations();
					$menu_obj        = get_term( $theme_locations[ $location_name ], 'nav_menu' );
					if ( isset( $menu_obj->name ) ):
						echo '<div class=" menu_overskrift">' . $menu_obj->name . '</div>';
					endif;
					wp_nav_menu( array( 'theme_location' => $location_name, 'fallback_cb' => false ) );
					?>

                </div>
            </div>
        </div>
    </div>
</footer>
<?php if ( defined( 'INIT_LIGHTBOX' ) && INIT_LIGHTBOX ): ?>
    <script type="text/javascript">document.write('<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls"><div class="slides"></div><h3 class="title"></h3><a class="prev">‹</a><a class="next">›</a><a class="close">×</a><a class="play-pause"></a></div>');</script>
<?php endif; ?>

<?php // m('popup'); ?>
<?php // m('popup', array('location' => 'side')); ?>

</div> <!-- outer-container slut -->

<?php wp_footer(); ?>
<!-- Start of Sleeknote signup and lead generation tool - www.sleeknote.com -->

<script id='sleeknoteScript' type='text/javascript'>
  (function () {
    var sleeknoteScriptTag = document.createElement('script');
    sleeknoteScriptTag.type = 'text/javascript';
    sleeknoteScriptTag.charset = 'utf-8';
    sleeknoteScriptTag.src = ('//sleeknotecustomerscripts.sleeknote.com/4352.js');
    var s = document.getElementById('sleeknoteScript');
    s.parentNode.insertBefore(sleeknoteScriptTag, s);
  })();
</script>

<!-- End of Sleeknote signup and lead generation tool - www.sleeknote.com -->
</body>
</html>