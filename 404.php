<?php if(!defined('CUSTOM_404') || (defined('CUSTOM_404') && !CUSTOM_404)):header("HTTP/1.0 301 Moved Permanently");header('Location: /');endif;
get_header(); ?>
	
<section id="content" class="page404">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><?php _e('Page not found...', 'onlineplus-general'); ?></h1>
                <p><?php echo __('The requested page or file, could not be found. Look in the menu or go to', 'onlineplus-general').' <a href="'.get_bloginfo('url').'">'.__('the front page', 'onlineplus-general').'</a>.'; ?></p>
            </div>
        </div>
    </div>	
</section>

<?php get_footer();