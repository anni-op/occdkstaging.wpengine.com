<?php 

$overskrift = get_sub_field('overskrift');
$indhold = get_sub_field('indhold');
$link = get_sub_field('link');

if($indhold || $indhold || $link):
  
    echo '<div class="col-xs-12 aside_kasse">';
        echo '<div class="h3">'.$overskrift.'</div>';
        echo '<div>'.$indhold.'</div>';

        
        if($link){
            echo '<div>';
                echo '<a class="button" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.($link['title'] ? $link['title'] : 'Læs mere' ).'</a>';
            echo '</div>';
        }
        
    echo '</div>';
           
endif;

?>