<?php

echo '<div class="usp_kasse">';
    echo '<div class="h3">';
         (get_sub_field('overskrift')?  the_sub_field('overskrift') : the_title());
    echo '</div>';

    if( have_rows('usp_er') ):
        echo '<ul class="usp_liste">';
        while ( have_rows('usp_er') ) : the_row();

            echo '<li>';
            the_sub_field('text');
            echo '</li>';

        endwhile;
        echo '</ul>';

        //mødelokaler
        $link = get_sub_field('link');
        if($link):
            echo '<a class="button outline" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.($link['title'] ? $link['title'] : 'Se muligheder' ).' <i class="fas fa-long-arrow-right"></i></a>';
        endif;

        //prisliste
        $pris = get_field('pris');
        $efter_pris = get_field('efter_pris');
        $under_pris = get_field('under_pris');
        if($pris):
            echo '<div class="pris text-center">'.$pris.' '.$efter_pris.'</div>';
            echo '<div class="under_pris text-center">'.$under_pris.'</div>';
        endif;

        $link = get_field('Link');
        if($link):
            echo '<a class="button" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.($link['title'] ? $link['title'] : 'Se muligheder' ).' <i class="fas fa-long-arrow-right"></i></a>';
        endif;

    else :

        // no rows found

    endif;

echo '</div>';

?>