<?php
	$menu_container_class = is_array($settings) && array_key_exists('menu_container_class', $settings) ? $settings['menu_container_class'] : '';
	echo '<div id="topmenu_wrap"><div class="'.$menu_container_class.'">';
		echo '<div class="menu_container">';
		$walker = new mega_menu();
		wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu_id' => 'navbar', 'items_wrap' => '<div id="%1$s" class="%2$s"><ul>%3$s</ul></div>', 'walker' => $walker ) );
		echo '</div>';
		echo '<div id="mobile_menu_container" class="text-right">';
		echo '<a href="#Mobile_Menu"><i class="fa fa-bars"></i></a>';
		echo '</div>';
	echo '</div></div>';