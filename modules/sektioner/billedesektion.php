<?php

$billede_1 = get_sub_field('billede_1');
$billede_2 = get_sub_field('billede_2');
$billede_3 = get_sub_field('billede_3');
if($billede_1 && $billede_2 && $billede_3):

echo '<section class="billedesektion">';
    echo '<div class="container">';
        echo '<div class="row">';
            echo '<div class="col-xs-12 col-sm-7 v_billede_col">';
               echo wp_get_attachment_image( $billede_1, 'billedesektion_big', '', array('class'=> 'billedesektion_1') );
            echo '</div>';
            echo '<div class="col-xs-12 col-sm-5 h_billede_col hidden-xs">';
                echo wp_get_attachment_image( $billede_2, 'billedesektion_small', '', array('class'=> 'billedesektion_2') );
                echo wp_get_attachment_image( $billede_3, 'billedesektion_small', '', array('class'=> 'billedesektion_3') );
            echo '</div>';

        echo '</div>';
    echo '</div>';
echo '</section>';

endif;

?>