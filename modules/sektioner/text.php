<?php 

$indhold = get_sub_field('indhold');

if($indhold):
    echo '<section class="text">';
        echo '<div class="container">';
            echo '<div class="row">';
                echo '<div class="col-xs-12">';
                echo $indhold;
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</section>';
endif;

?>