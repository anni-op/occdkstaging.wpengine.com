<?php 

$galleri = get_sub_field('billeder');

if($galleri):
    echo '<section class="slideshow">';
        echo '<div class="container">';
            echo '<div class="row">';
                echo '<div class="galleri_slider">';
                foreach( $galleri as $image ):
                    echo '<div class="col-xs-12 galleri_item">';
                        echo '<div class="galleri_wrapper">';
                            echo wp_get_attachment_image( $image, 'slideshow' ); 
                        echo '</div>';
                    echo '</div>';
                endforeach;
                echo '</div>';
                echo '<div class="col-xs-12 op_slider_arror_wrapper">';
                    echo '<div class="op_slider_arror">';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</section>';
endif;
?>