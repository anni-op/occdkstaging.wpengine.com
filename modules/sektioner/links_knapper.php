<?php

$links_knapper = get_sub_field('knapper');

if($links_knapper):

echo '<section class="knapper_links">';
    echo '<div class="container">';
        echo '<div class="row">';

           
            if(have_rows('knapper')): 
                while(have_rows('knapper')): the_row();
                  
                   $link = get_sub_field('link');
                   if($link){
                       echo '<div class="col-xs-12 col-sm-6 knapper_links_item">';
                       echo '<a class="button" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.($link['title'] ? $link['title'] : 'Læs mere' ).' <i class="fas fa-long-arrow-right"></i></a>';
                       echo '</div>';
                   }
                  
                endwhile;
            else: 
            endif; 
            

        echo '</div>';
    echo '</div>';
echo '</section>';

endif;