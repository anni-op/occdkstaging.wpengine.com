<?php

$events = get_sub_field('events');
$overskrift = get_sub_field('overskrift');

if($overskrift || $events):
    echo '<section class="events">';
        echo '<div class="container">';
            echo '<div class="row">';
                echo '<div class="col-xs-7 col-sm-6 h2">';
                    echo $overskrift;
                echo '</div>';

                echo '<div class="col-xs-5 col-sm-6 text-right">';
                   
                    echo '<a href="'. get_post_type_archive_link( 'events' ).'">'.__('Se alle events', 'onlineplus-general').' <i class="fas fa-long-arrow-right"></i></a>';
                echo '</div>';
            echo '</div>';
            echo '<div class="row">'; 
                $object = get_sub_field('events');
                if($object): 
                    foreach($object as $post): setup_postdata($post);
                        get_template_part('loop','event');
                    endforeach;
                    wp_reset_postdata();
                else: 

                   

                endif; 
              

            echo '</div>';
        echo '</div>';
    echo '</section>';
endif;

?>