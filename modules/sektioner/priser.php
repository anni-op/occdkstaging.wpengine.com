<?php 

$indledning = get_sub_field('indledning');
$priskasser = get_sub_field('priskasser');

if($priskasser):
    echo '<section class="priskasser">';
        echo '<div class="container">';
            echo '<div class="row">';
                echo '<div class="col-xs-12">';
                    echo $indledning;
                echo '</div>';

                
                $object = get_sub_field('priskasser');
                if($object): 
                    foreach($object as $post): setup_postdata($post);
                        echo '<div class="col-xs-12 col-sm-4 pris_item">';
                            get_template_part('modules/usp_repeater');
                        echo '</div>';
                    endforeach;
                    wp_reset_postdata();
                else: 
                endif; 
            
              
                
               
                
            echo '</div>';
        echo '</div>';
    echo '</section>';
endif;

?>