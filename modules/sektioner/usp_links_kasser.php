<?php 

$indledning = get_sub_field('indledning');
$usp_kasser = get_sub_field('usp_kasser');


if($usp_kasser):

    echo '<section class="usp_links_kasser">';
        echo '<div class="container">';
            echo '<div class="row">';
                echo '<div class="col-xs-12">';
                    echo $indledning;
                echo '</div>';

               

                
                if(have_rows('usp_kasser')): 
                    while(have_rows('usp_kasser')): the_row();

                        $billede = get_sub_field('billede');
                        $overskrift = get_sub_field('overskrift');
                        $link = get_sub_field('link');
                       

                        echo '<div class="col-xs-12 col-sm-4 usp_links_kasse_item">';

                        echo '<div class="img_wrapper">';
                            if($link):
                                echo '<a class="" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >';
                            endif;
                                echo wp_get_attachment_image( $billede, 'link_bokse' );
                            if($link):
                                echo '</a>';
                            endif;
                        echo '</div>';
                        //echo '<div class="overskrift">'.$overskrift.'</div>';
                        get_template_part('modules/usp_repeater');
                       
                        echo '</div>';


                    endwhile;
                else: 
                endif; 
                

                

            echo '</div>';
        echo '</div>';
    echo '</section>';

endif;

?>