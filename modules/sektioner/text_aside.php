<?php 


$indhold = get_sub_field('indhold');
$læs_mere = get_sub_field('laes_mere');
// $sideindhold = have_rows('sideindhold');



if($indhold):
    echo '<section class="text_aside">';
        echo '<div class="container">';
            echo '<div class="row">';

                echo '<div class="col-xs-12 col-sm-8 v-indhold">';
                    
                    echo 
                    '<div class="indhold '.($læs_mere ? 'read_more' : '').'">'.
                        $indhold.
                        ($læs_mere ? '<span class="read_more_btn" data-more="'.__('Read more', 'onlineplus-general').'" data-less="'.__('Læs mindre', 'onlineplus-general').'">'.__('Read more', 'onlineplus-general').'</span>' : '').
                    '</div>'; 

                    //repeater
                    if(have_rows('knapper')): 
                        echo '<div class="row link_wrapper">';
                        while(have_rows('knapper')): the_row();
                           
                            $link = get_sub_field('knap');
                           
                            if($link){
                                
                                $length = strlen('.pdf');
                                if(substr($link['url'], -$length) === '.pdf'):
                                    echo '<div class="col-sm-12 col-sm-4"><a class="button pdf" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' ><i class="fas fa-file-pdf"></i> '.($link['title'] ? $link['title'] : 'Læs mere' ).'</a></div>';
                                else:
                                    echo '<div class="col-sm-12 col-sm-4"><a class="button url" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.($link['title'] ? $link['title'] : 'Læs mere' ).' <i class="far fa-long-arrow-alt-right"></i></a></div>';
                                endif;

                                
                            }
                           
                        endwhile;
                        echo '</div>';
                    else: 
                      
                    endif; 
                    


                echo '</div>';

                echo '<div class="col-xs-12 col-sm-4 h-indhold">';



                //Flexibel content
                if( have_rows('sideindhold') ):

                    // loop through the rows of data
                   while ( have_rows('sideindhold') ) : the_row();
               
                       if( get_row_layout() == 'usp' ):
                            get_template_part('modules/usp_repeater');
                        elseif( get_row_layout() == 'kasse' ): 

                            get_template_part('modules/kasse_repeater'); 

                       endif;                           
                       
               
                   endwhile;
               
               else :
               
                   // no layouts found
               
               endif;




                    
                    
                echo '</div>';

            echo '</div>';
        echo '</div>';
    echo '</section>';
endif;
?>