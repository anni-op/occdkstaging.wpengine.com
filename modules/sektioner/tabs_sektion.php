<?php 

$indledning = get_sub_field('overskrift');
$tabs = get_sub_field('tabs');

$tabs_buttons = '';
$tabs_content = '';

if($indledning && $tabs):


    
    if(have_rows('tabs')): 
        $i = 0;
        while(have_rows('tabs')): the_row(); 
            

            $tabs_buttons .=  '<div class="button" data-filter=".tab_'.$i.'">'.get_sub_field('tab_overskrift').'</div>';


            
            if(have_rows('tab_indhold')): 
                $tabs_content .='<div class="tab_item tab_'.$i.'" data-tab-indhold="'.$i.'">';
                while(have_rows('tab_indhold')): the_row();

                    $billede = get_sub_field('billede');
                    $over_overskrift = get_sub_field('over_overskrift');
                    $overskrift = get_sub_field('overskrift');
                    $Indledning = get_sub_field('indledning');
                    $link = get_sub_field('link');

                    $tabs_content .= '<div class="row tab_content" >';
                        $tabs_content .= '<div class="col-xs-12 col-sm-6 image_col" >';
                        if($link):
                            $tabs_content .='<a class="" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >';
                        endif;
                                $tabs_content .=  wp_get_attachment_image( $billede, 'tabs' );
                        if($link):
                            $tabs_content .= '</a>';
                        endif;
                        $tabs_content .= '</div>';
                        $tabs_content .= '<div class="col-xs-12 col-sm-6 info_col" >';

                            $tabs_content .= '<div class="over_overskrift">'.$over_overskrift .'</div>';
                            $tabs_content .= '<div class="overskrift">'.$overskrift .'</div>';
                            $tabs_content .= '<div class="indledning">'.$Indledning .'</div>';
                           
                            
                            if($link){
                                $tabs_content .= 
                                '<div class="link">'.
                                    '<a class="" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.
                                        ($link['title'] ? $link['title'] : 'Læs mere' ). ' <i class="fas fa-long-arrow-right"></i>'.
                                    '</a>'
                                .'</div>';
                            }
                            
                            

                        $tabs_content .= '</div>';
                    $tabs_content .= '</div>';

                endwhile;
                $tabs_content .='</div>';
            else: 
            endif; 
           
            $i++;
        endwhile;
    else: 
    endif; 
  



    echo '<section class="tabs_indledning">';
        echo '<div class="container">';
            echo '<div class="row">';

                echo '<div class="col-xs-12">';
                    echo $indledning;
                echo '</div>';

                echo '<div class="col-xs-12 tabs_button_wrapper">';
                    echo $tabs_buttons;
                echo '</div>';

            echo '</div>';
        echo '</div>';
    echo '</section>';

    echo '<section class="tabs_content">';
        echo '<div class="container">';
            //echo '<div class="row">';
                echo $tabs_content;
            //echo '</div>';
        echo '</div>';
    echo '</section>';





endif;

?>