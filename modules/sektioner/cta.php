<?php

$overskrift = (get_sub_field('overskrift') ? get_sub_field('overskrift') : get_field('cta_overskrift', 'option') );
$link = (get_sub_field('knap') ? get_sub_field('knap') : get_field('cta_knap', 'option') );

if($overskrift):
    echo '<section class="cta">';
    echo '<div class="container">';
        echo '<div class="bg">';
            echo '<div class="row">';
                       
                    echo '<div class="col-xs-12 col-sm-7 overskrift">';
                        echo ''.$overskrift.'';
                    echo '</div>';
                    echo '<div class="col-xs-12 col-sm-5 button_col" >';
                        
                        if($link){
                            echo '<a class="button" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.($link['title'] ? $link['title'] : 'Læs mere' ).'</a>';
                        }
                    echo '</div>';
            echo '</div>';
        echo '</div>';
    echo '</div>';
    echo '</section>';
endif;


?>