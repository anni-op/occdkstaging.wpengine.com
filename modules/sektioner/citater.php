<?php
$object = get_sub_field('citat');
if($object): 
    echo '<section class="citater">';
        echo '<div class="container">';
            
            echo '<div class="row ">';             
                echo '<div class="citat_slider">';
                    foreach($object as $post): setup_postdata($post);
                        
                        $afsender = get_field('citat_afsender');
                        if(!$afsender ):
                            $afsender = get_the_title();
                        endif;

                        $stilling = get_field('citat_stilling');

                        echo '<div class="citat_wrapper"><div class="col-xs-12 col-sm-8 col-sm-offset-2 citat-item">';

                            echo '<div class="quote">';
                                echo '<span>&rdquo;</span>';
                            echo '</div>';

                            echo '<div class="citat text-center">';
                                echo ''.get_the_content().'';
                            echo '</div>';

                            echo '<div class="citat_info ">';
                                echo '<div class="citat_info_wrapper">';
                                    //echo '<div class="circle"></div>';
                                    
                                    echo '<div class="citat_afsender">'.$afsender.'</div>';
                                
                                    if($stilling):
                                        echo '<div class="citat_stilling">'.$stilling.'</div>';
                                    endif;
                                echo '</div>';
                            echo '</div>';

                        echo '</div></div>';
                    endforeach;
                    wp_reset_postdata();
                echo '</div>';

                echo '<div class="col-xs-12 op_slider_arror_wrapper">';
                    echo '<div class="op_slider_arror">';
                    echo '</div>';
                echo '</div>';

            echo '</div>';
        echo '</div>';
    echo '</section>';
        
else: 

endif; 
?>

