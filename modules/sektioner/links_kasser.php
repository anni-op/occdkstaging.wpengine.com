<?php 
$overskrift = get_sub_field('overskrift');
$link_bokse = get_sub_field('link_bokse');

if($link_bokse):

    echo '<section class="link_bokse">';
        echo '<div class="container">';
            echo '<div class="row">';
                echo '<div class="col-xs-12 h2">';
                    echo $overskrift;
                echo '</div>';

               
                if(have_rows('link_bokse')): 
                    while(have_rows('link_bokse')): the_row();
                        $link = get_sub_field('link');
                        $billede = get_sub_field('billede');
                        echo '<div class="col-xs-12 col-sm-6 link_boks_item">';
                        echo '<a class="" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >';
                            echo '<div class="img_wrapper">'.wp_get_attachment_image( $billede, 'link_bokse' ).'</div>';
                            echo '<div class="title">'.($link['title'] ? $link['title'] : 'Læs mere' ).'</div>';
                        echo '</a>';
                        echo '</div>';

                    

                    endwhile;
                else: 
                endif; 
               

            echo '</div>';
        echo '</div>';
    echo '</section>';
endif;

?>