<?php
// check if the flexible content field has rows of data
if( have_rows('sektioner') ):

     // loop through the rows of data
    while ( have_rows('sektioner') ) : the_row();

        if( get_row_layout() == 'billedesektion' ):

            get_template_part('modules/sektioner/billedesektion');

            
        elseif( get_row_layout() == 'text-aside' ): 

            get_template_part('modules/sektioner/text_aside'); 
        
        elseif( get_row_layout() == 'slideshow' ): 

            get_template_part('modules/sektioner/slideshow'); 
        
        elseif( get_row_layout() == 'citater' ): 

            get_template_part('modules/sektioner/citater'); 

        elseif( get_row_layout() == 'cta' ): 

            get_template_part('modules/sektioner/cta'); 

            
        elseif( get_row_layout() == 'tabs_sektion' ): 

            get_template_part('modules/sektioner/tabs_sektion'); 

        elseif( get_row_layout() == 'text' ): 

            get_template_part('modules/sektioner/text');         

        elseif( get_row_layout() == 'links_kasser' ): 

            get_template_part('modules/sektioner/links_kasser'); 
        
        elseif( get_row_layout() == 'usp_links_kasser' ): 

            get_template_part('modules/sektioner/usp_links_kasser'); 

        elseif( get_row_layout() == 'priser' ): 

            get_template_part('modules/sektioner/priser'); 
        
        elseif( get_row_layout() == 'links_knapper' ): 

            get_template_part('modules/sektioner/links_knapper'); 
        
        elseif( get_row_layout() == 'udvalgte_events' ): 

            get_template_part('modules/sektioner/udvalgte_events'); 
    
        endif;

        

    endwhile;

else :

    // no layouts found

endif;

?>