<?php if(post_password_required()):
	return;
endif; ?>
<?php if(have_comments()): ?>
<?php function op_comments($comment, $args, $depth){
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

		<?php $comment_id = get_comment_ID(); echo get_avatar(get_comment($comment_id)->user_id, 48); ?>
		<div class="comment-content clearfix">
			<p><strong><?php printf(__('%s', 'onlineplus-general'), get_comment_author_link()); ?></strong> <span class="date text-muted pull-right"><?php printf(__('%1$s', 'onlineplus-general'), get_comment_date('j\. M Y'), get_comment_time()); ?></span></p>

			<div class="comment-text">
				<?php comment_text(); ?>
			</div>

			<div class="reply fa fa-reply pull-right">
				<?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
			</div>
		</div>
		
		<div class="clearfix"></div>
<?php } ?>
<h2 class="comments-title">
	<?php printf(__('%1$s thought'.(get_comments_number()>1?'s':'').' on &ldquo;%2$s&rdquo;', 'onlineplus-general'), number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' ); ?>
</h2>

<ol class="comment-list">
	<?php wp_list_comments(array('reply_text' => __('Reply', 'onlineplus-general'), 'style' => 'ol', 'callback' => 'op_comments')); ?>
</ol>

<?php if(get_comment_pages_count() > 1 && get_option('page_comments')): ?>
	<ul class="pager">
		<li><?php previous_comments_link(__('&laquo; Older comments', 'onlineplus-general')); ?></li>
		<li><?php next_comments_link(__('Newer comments &raquo;', 'onlineplus-general')); ?></li>
	</ul>
<?php endif; ?>

<?php if(!comments_open() && get_comments_number()): ?>
<p class="nocomments"><?php _e('Comments closed.', 'onlineplus-general'); ?></p>
<?php endif; endif;
$args = array(
	'id_form'           => 'commentform',
	'id_submit'         => 'submit',
	'title_reply'       => __('Your thoughts', 'onlineplus-general'),
	'title_reply_to'    => __('Reply to %s', 'onlineplus-general'),
	'cancel_reply_link' => __('Cancel', 'onlineplus-general'),
	'label_submit'      => __('Send', 'onlineplus-general'),

	'comment_field' => '<label for="comment">'.__('Comment', 'onlineplus-general').
		'</label><textarea id="comment" name="comment" aria-required="true">'.
		'</textarea>',

	'must_log_in' => '<p class="must-log-in">'.
		sprintf(
			__('You must be <a href="%s">logged in</a> to comment.', 'onlineplus-general'),
			wp_login_url(apply_filters('the_permalink', get_permalink()))
		).'</p>',

	'logged_in_as' => '<p class="logged-in-as">' .
		sprintf(
		__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out">Log out?</a>', 'onlineplus-general'),
			admin_url('profile.php'),
			$user_identity,
			wp_logout_url( apply_filters('the_permalink', get_permalink()))
		).'</p>',

	'comment_notes_before' => '<p class="comment-notes">'.
		__('Your e-mail address will not be published.', 'onlineplus-general').($req?'<span class="required">*</span>':'').
		'</p>',

	'comment_notes_after' => '',

	'fields' => apply_filters('comment_form_default_fields', array(

		'author' =>
			'<label for="author">'.__('Name', 'onlineplus-general').($req?'<span class="required">*</span>':'').'</label>'.
			'<input id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).
			'" size="30" aria-required="true" />',

		'email' =>
			'<label for="email">'.__('E-mail', 'onlineplus-general').($req?'<span class="required">*</span>':'').'</label>'.
			'<input id="email" name="email" type="text" value="'.esc_attr($commenter['comment_author_email']).
			'" size="30" aria-required="true" />',

		'url' =>
			'<label for="url">' .
			__('Website', 'onlineplus-general').'</label>'.
			'<input id="url" name="url" type="text" value="'.esc_attr($commenter['comment_author_url']).
			'" size="30" />'
		)
	),
  );
comment_form($args);