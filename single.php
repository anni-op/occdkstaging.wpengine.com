<?php if(!defined('NEWS') || (defined('NEWS') && !NEWS)): header("HTTP/1.0 404 Not Found"); get_template_part('404'); die(); endif; ?>
<?php get_header(); ?>
			
<?php m('banner', array('banner_type' => 'full_width')); ?>
	
<section id="content" <?php post_class(); ?>>
    <div class="container">
	    <div class="row">

          
            <?php   
            if(has_post_thumbnail()):
                   
                    $full_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'slideshow');
                    echo '<div class="col-xs-12 single_thumbnail_w">';
                        echo '<img src="'.$full_image[0].'" alt="" class="alignleft" />';
                    echo '</div>';
            endif;
            ?>

         
            <div class="col-xs-12 col-sm-8 singel_content">

		<?php if(have_posts()): while(have_posts()): the_post(); ?>
                    <article>
			<h1><?php the_title(); ?></h1>
			<p class="date text-muted"><?php the_date('d. M Y'); ?></p>
                        
			<?php  the_content(); ?>
                    </article>
			<?php m('comments'); ?>

                <?php endwhile; endif; ?>
            </div>
            
            <div class="col-xs-12 col-sm-4 single_aside">
                <div class="h3"><?php echo __('Seneste nyheder:', 'onlineplus-general'); ?></div>
                <div class="row">                
                <?php 
                 $container_col = 'col-xs-12';
                 set_query_var( 'container_col', $container_col );

                $the_query = new WP_Query( array(  'post_type' => 'post', 'posts_per_page' => '2'));

                if ( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        get_template_part('loop', 'nyhed');
                    endwhile;
                    wp_reset_postdata();
                endif;


                ?>
                </div>
            </div>
            
        </div>        
    </div>
</section>

<?php get_footer();