<?php if(!defined('NEWS') || (defined('NEWS') && !NEWS)): header("HTTP/1.0 404 Not Found"); get_template_part('404'); die(); endif; ?>
<?php get_header(); ?>
			
<?php m('banner', array('banner_type' => 'full_width')); ?>
	
<section id="content" <?php post_class(); ?>>
    <div class="container">
	<div class="row">
            <div class="col-xs-12">
		<?php if(have_posts()): while(have_posts()): the_post(); ?>
                    <article>
			<h1><?php the_title(); ?></h1>
			<p class="date text-muted"><?php the_date('d-m-Y'); ?></p>
                        <?php   
                        if(has_post_thumbnail()):
                                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'medium');
                                $full_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full');
                                echo '<a href="'.$full_image[0].'" data-gallery=""><img src="'.$thumbnail[0].'" alt="" class="alignleft" /></a>';
                        endif;?>
			<?php  the_content(); ?>
                    </article>
			<?php m('comments'); ?>

                <?php endwhile; endif; ?>
            </div>
            
            <div class="col-xs-12">
                <?php m('gallery', array('per_row' => 6, 'img_style' => 'thumbnail')); ?>
            </div>
            
        </div>        
    </div>
</section>

<?php get_footer();