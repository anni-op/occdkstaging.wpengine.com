<?php 
$terms = get_the_terms(get_the_ID(), 'events-kategori');

if($terms):
$terms_list =  implode(' ', array_map(function ($entry) {
    return $entry->slug;
  }, $terms));
else:
    $terms_list = '';
endif;


echo '<div class="col-xs-12 col-sm-4 event_item '.$terms_list.'" >';
    echo '<a href="'.get_the_permalink().'">';
        if(has_post_thumbnail()):
            $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'nyheder');   
            echo '<div class="event_image">';                   
                echo '<img src="'.$thumbnail[0].'" alt="" class="" />';
            echo '</div>';
        endif;
        echo '<div class="bg">';
        echo  '<strong>'.get_the_title().'</strong>';
    
        $dato = get_field('dato'); 
        $s_dato = get_field('slut_dato');
        if($dato ):?>
            <div>
                <strong><?php echo __('Dato', 'onlineplus-general');?>:</strong> 
                <?php echo $dato; ?>

                <?php
                    if($s_dato != $dato && $s_dato):
                        echo '- '. $s_dato;
                    endif;
                ?>
            </div>

        <?php endif;
        echo '</div>';
        echo '</a>';
        echo '</div>'; ?>