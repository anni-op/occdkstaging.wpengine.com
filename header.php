<!DOCTYPE html>
<!--
   ____        _ _            _____  _                 
  / __ \      | (_)          |  __ \| |            _   
 | |  | |_ __ | |_ _ __   ___| |__) | |_   _ ___ _| |_ 
 | |  | | '_ \| | | '_ \ / _ \  ___/| | | | / __|_   _|
 | |__| | | | | | | | | |  __/ |    | | |_| \__ \ |_|  
  \____/|_| |_|_|_|_| |_|\___|_|    |_|\__,_|___/      
-->
<html lang="da-DK">
    <head>
        <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="format-detection" content="telephone=no" />
		<meta name="theme-color" content="#ddd">
		<meta name="msapplication-navbutton-color" content="#ddd">
		<base href="<?php echo get_stylesheet_directory_uri(); ?>/" />
		<title><?php wp_title(''); ?></title>

		<?php wp_head(); ?>
		
		<?php if(defined('AUTHOR_URL') && AUTHOR_URL != ''): ?><link rel="author" href="<?php echo AUTHOR_URL; ?>" /><?php endif; ?>
		<!--<link rel="shortcut icon" href="images/favicon.png" />-->
		
		<?php m('google_analytics'); ?>
    </head>


	<?php 
	 
    /*$primaerfarve = get_field('primaerfarve');
    
    $blue_pages = array(8273, 8314, 8313, 8312, 8311, 8310, 8309, 8308, 8306);
	if(is_page($blue_pages)):
		$color_class = 'prim_blue'; 
	else: 
		$color_class = 'prim_purple';
    endif;*/  
    
    $primaerfarve = get_field('primaer_farve'); 



    if($primaerfarve == 'blaa'):
        $color_class = 'prim_blue'; 
    elseif($primaerfarve == 'graa'):
        $color_class = 'prim_graa'; 
    else:
        $color_class = 'prim_purple'; 
    endif;

	
	?>

    <body <?php body_class($color_class); ?>>
    <?php
        pre('hello 2'); 
        pre($primaerfarve);  
    ?>
        <div id="outer-container" x-ms-format-detection="none"> <!-- Bliver afsluttet i footer -->
            <?php if(defined('FACEBOOK_SCRIPTS') && FACEBOOK_SCRIPTS !== false) : ?>
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/da_DK/sdk.js#xfbml=1&version=v2.7";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <?php endif; ?>
            
            

            <header id="header">

                <div class="header_top hidden-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6"><?php do_action('wpml_add_language_selector'); ?></div>
                            <div class="col-xs-6 col-sm-6">
                            <?php
                                $location_name = 'top-menu';
                                $theme_locations = get_nav_menu_locations();
                                $menu_obj = get_term( $theme_locations[$location_name], 'nav_menu' );
                                // if(isset($menu_obj->name)):
                                //     echo '<div class="h3">'.$menu_obj->name.'</div>';
                                // endif;
                               
                                    wp_nav_menu( array( 'theme_location' => $location_name, 'fallback_cb' => false ) );
                                
                            ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container main_header">
                    <div class="row flex-center">
                        <div class="col-xs-3 col-sm-3 col-md-1 col-lg-3">
                            <?php 
                            
                                $logo = get_field('logo_sort', 'option');
                                if($logo):
                                    echo '<a href="'.get_bloginfo('url').'">'.wp_get_attachment_image( $logo, 'full', '', array('id' => 'logo') ).'</a>';
                                else: ?>
                                    <a href="<?php bloginfo('url'); ?>" id="logo"><img src="assets/images/logo.png" alt="<?php bloginfo('name'); ?>"></a>
                                <?php endif; ?>
                            
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-11 col-lg-9 menu_wrapper">
                            <?php //echo add_search_box_to_menu();?>

                            <div class="search_mobile visible-xs-inline visible-sm-inline">                                
                                    <span  class='search_btn'>
                                        <i class='fa fa-search' aria-hidden='true'></i>                        
                                    </span>
                                    <div class='search_field'><div class='container'><div class='row'><div class='col-xs-12'><?php get_search_form(); ?></div></div></div>
                                    </div>
                            </div>

                            <?php

                            $location_name = 'main_menu';
                            $theme_locations = get_nav_menu_locations();
                            $menu_obj = get_term( $theme_locations[$location_name], 'nav_menu' ); 
                            
                            $link = get_field('book_hotel', $menu_obj);  
                            if($link):
                                echo '<div class="mobile_book visible-xs-inline visible-sm-inline">';
                                    echo '<a href="'.$link['url'].'" target='.($link['target'] ? $link['target'] : '_self' ).' class="button">';
                                    echo ($link['title'] ? $link['title'] : 'Læs mere' );
                                    echo '</a>';
                                echo '</div>';
                            endif;

                            ?>

                            <?php
                            $link = get_field('field_name');
                            if($link){
                                echo '<a class="" href='.$link['url'].' target='.($link['target'] ? $link['target'] : '_self' ).' >'.($link['title'] ? $link['title'] : 'Læs mere' ).'</a>';
                            }
                            ?>
                            

                            <?php 
                                $nature_energi = 8273; 
                                $show_menu = true;
                                $ancestors = get_post_ancestors($post->ID);
                                foreach($ancestors as $a):
                                  
                                    if($a == $nature_energi):
                                        $show_menu = false;
                                    endif;
                                    
                                endforeach;
                                    
                                


                                if(is_page($nature_energi) || $show_menu == false ):
                                    //dont show menu
                                else:
                                    m('topmenu', array('menu_container_class' => '')); 
                                endif;
                            ?>
                        </div>
                    </div>
                </div>
            </header>
            <div class="cheat-fix"></div>

            <?php 
            // if(!is_front_page() && !is_home() && function_exists('yoast_breadcrumb')):
            //         yoast_breadcrumb('<p id="breadcrumbs" class="container">','</p>');
            // endif;

            do_action('deprecated_browser_notice');